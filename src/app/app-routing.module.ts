import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EnregistrementAuditComponent } from './views/enregistrement-audit/enregistrement-audit.component';
import { HomeComponent } from './views/home/home.component';
import { LoginComponent } from './views/login/login.component';
import { TriAuditComponent } from './views/tri-audit/tri-audit.component';
import { UsersListingComponent } from './views/admin/users/users-listing/users-listing.component';
import { EquipesListingComponent } from './views/admin/users/equipes-listing/equipes-listing.component';
import { ArticlesListingComponent } from './views/admin/articles/articles-listing/articles-listing.component';
import { ModelesListingComponent } from './views/admin/articles/modeles-listing/modeles-listing.component';
import { CriteresListingComponent } from './views/admin/criteres/criteres-listing/criteres-listing.component';
import { TypesCriteresListingComponent } from './views/admin/criteres/types-criteres-listing/types-criteres-listing.component';
import { ProcessListingComponent } from './views/admin/criteres/process-listing/process-listing.component';
import { ObjectifAnnuelListingComponent } from './views/admin/audit/objectif-annuel-listing/objectif-annuel-listing.component';
import { TypesTrisListingComponent } from './views/admin/tris/types-tris-listing/types-tris-listing.component';
import { AvoListingComponent } from './views/admin/tris/avo-listing/avo-listing.component';
import { PostEquipeComponent } from './views/admin/users/post-equipe/post-equipe.component';
import { PostUsersComponent } from './views/admin/users/post-users/post-users.component';
import { TypesTrisPostComponent } from './views/admin/tris/types-tris-post/types-tris-post.component';
import { AvoPostComponent } from './views/admin/tris/avo-post/avo-post.component';
import { TypesCriteresPostComponent } from './views/admin/criteres/types-criteres-post/types-criteres-post.component';
import { ProcessPostComponent } from './views/admin/criteres/process-post/process-post.component';
import { CriteresPostComponent } from './views/admin/criteres/criteres-post/criteres-post.component';
import { ObjectifAnnuelPostComponent } from './views/admin/audit/objectif-annuel-post/objectif-annuel-post.component';
import { ModelesPostComponent } from './views/admin/articles/modeles-post/modeles-post.component';
import { ArticlesPostComponent } from './views/admin/articles/articles-post/articles-post.component';
import { CrashQualiteComponent } from './views/crash-qualite/crash-qualite.component';
import { OpenCrashQualiteComponent } from './views/open-crash-qualite/open-crash-qualite.component';
import { ZonesComponent } from './views/admin/crashQualite/zones/zones.component';
import { LignesComponent } from './views/admin/crashQualite/lignes/lignes.component';
import { MachinesComponent } from './views/admin/crashQualite/machines/machines.component';
import { ZonesPostComponent } from './views/admin/crashQualite/zones-post/zones-post.component';
import { LignesPostComponent } from './views/admin/crashQualite/lignes-post/lignes-post.component';
import { MachinesPostComponent } from './views/admin/crashQualite/machines-post/machines-post.component';
import { ModelesUpdateComponent } from './views/admin/articles/modeles-update/modeles-update.component';
import { ArticlesUpdateComponent } from './views/admin/articles/articles-update/articles-update.component';
import { LignesUpdateComponent } from './views/admin/crashQualite/lignes-update/lignes-update.component';
import { ZonesUpdateComponent } from './views/admin/crashQualite/zones-update/zones-update.component';
import { MachinesUpdateComponent } from './views/admin/crashQualite/machines-update/machines-update.component';
import { TypesCriteresUpdateComponent } from './views/admin/criteres/types-criteres-update/types-criteres-update.component';
import { ProcessUpdateComponent } from './views/admin/criteres/process-update/process-update.component';
import { CriteresUpdateComponent } from './views/admin/criteres/criteres-update/criteres-update.component';
import { AvoUpdateComponent } from './views/admin/tris/avo-update/avo-update.component';
import { TypesTrisUpdateComponent } from './views/admin/tris/types-tris-update/types-tris-update.component';
import { EquipesUpdateComponent } from './views/admin/users/equipes-update/equipes-update.component';
import { UsersUpdateComponent } from './views/admin/users/users-update/users-update.component';
import { AuthGuardService as AuthGuard } from './services/auth-guard/auth-guard.service';
import { ClosedCrashQualiteComponent } from './views/closed-crash-qualite/closed-crash-qualite.component';

const routes: Routes = [
  { path: 'tri', component: TriAuditComponent, canActivate: [AuthGuard] },
  {
    path: 'tri/:id',
    component: TriAuditComponent,  canActivate: [AuthGuard],
  },
  {
    path: 'audit',
    component: EnregistrementAuditComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'audit/:id',
    component: EnregistrementAuditComponent,
    canActivate: [AuthGuard],
  },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'crash', component: CrashQualiteComponent, canActivate: [AuthGuard] },
  {
    path: 'crash/open/:id',
    component: OpenCrashQualiteComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'crash/closed/:id',
    component: ClosedCrashQualiteComponent,
    canActivate: [AuthGuard],
  },
  { path: 'admin/audit/objectif', component: ObjectifAnnuelListingComponent },
  { path: 'admin/audit/objectif/new', component: ObjectifAnnuelPostComponent },
  { path: 'admin/tri/typeTri', component: TypesTrisListingComponent },
  { path: 'admin/tri/typeTri/new', component: TypesTrisPostComponent },
  { path: 'admin/tri/typeTri/update/:id', component: TypesTrisUpdateComponent },
  { path: 'admin/tri/AVO', component: AvoListingComponent },
  { path: 'admin/tri/AVO/new', component: AvoPostComponent },
  { path: 'admin/tri/AVO/update/:id', component: AvoUpdateComponent },
  { path: 'admin/criteres', component: CriteresListingComponent },
  { path: 'admin/criteres/new', component: CriteresPostComponent },
  { path: 'admin/criteres/update/:id', component: CriteresUpdateComponent },
  {
    path: 'admin/criteres/typesCriteres',
    component: TypesCriteresListingComponent,
  },
  {
    path: 'admin/criteres/typesCriteres/new',
    component: TypesCriteresPostComponent,
  },
  {
    path: 'admin/criteres/typesCriteres/update/:id',
    component: TypesCriteresUpdateComponent,
  },
  { path: 'admin/criteres/process', component: ProcessListingComponent },
  { path: 'admin/criteres/process/new', component: ProcessPostComponent },
  {
    path: 'admin/criteres/process/update/:id',
    component: ProcessUpdateComponent,
  },
  { path: 'admin/articles', component: ArticlesListingComponent },
  { path: 'admin/articles/new', component: ArticlesPostComponent },
  { path: 'admin/articles/update/:id', component: ArticlesUpdateComponent },
  { path: 'admin/articles/modeles', component: ModelesListingComponent },
  { path: 'admin/articles/modeles/new', component: ModelesPostComponent },
  {
    path: 'admin/articles/modeles/update/:id',
    component: ModelesUpdateComponent,
  },
  { path: 'admin/users', component: UsersListingComponent },
  { path: 'admin/users/new', component: PostUsersComponent },
  { path: 'admin/users/update/:id', component: UsersUpdateComponent },
  { path: 'admin/users/equipes', component: EquipesListingComponent },
  { path: 'admin/users/equipes/new', component: PostEquipeComponent },
  { path: 'admin/users/equipes/update/:id', component: EquipesUpdateComponent },
  { path: 'admin/crashQualite/zones', component: ZonesComponent },
  { path: 'admin/crashQualite/zones/new', component: ZonesPostComponent },
  {
    path: 'admin/crashQualite/zones/update/:id',
    component: ZonesUpdateComponent,
  },
  { path: 'admin/crashQualite/lignes', component: LignesComponent },
  { path: 'admin/crashQualite/lignes/new', component: LignesPostComponent },
  {
    path: 'admin/crashQualite/lignes/update/:id',
    component: LignesUpdateComponent,
  },
  { path: 'admin/crashQualite/machines', component: MachinesComponent },
  { path: 'admin/crashQualite/machines/new', component: MachinesPostComponent },
  {
    path: 'admin/crashQualite/machines/update/:id',
    component: MachinesUpdateComponent,
  },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
