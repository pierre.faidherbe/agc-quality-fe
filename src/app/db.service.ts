import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {} from './components/top-form-audit/top-form-audit.component';
import {API_URL} from "./app.globalConstante";

@Injectable({
  providedIn: 'root',
})
export class DbService {
  private options = {
    headers: new HttpHeaders().set('Content-Type', 'application/json'),
  };

  constructor(private http: HttpClient) {}

  /* -------------------------------------------------------------------------- */
  /*                                    GETS                                    */

  /* -------------------------------------------------------------------------- */

  getCodeArticles(): Observable<CodeArticle[]> {
    return this.http.get<CodeArticle[]>(
      `${API_URL}/articles/codesArticles`
    );
  }

  getArticles = (): Observable<Article[]> => {
    return this.http.get<Article[]>(`${API_URL}/articles`);
  };

  getCodeArticle(codeArticle: number): Observable<Article> {
    console.log(codeArticle);
    return this.http.get<Article>(
      `${API_URL}/articles/${codeArticle}`
    );
  }

  getCodeArticlesPerModele(modele: Modele): Observable<Article[]> {
    return this.http.get<Article[]>(
      `${API_URL}/articles/codesArticles/modele/${modele.modeleId}`
    );
  }

  getAuditProcess(): Observable<Process[]> {
    return this.http.get<Process[]>(`${API_URL}/criteres/process`);
  }

  getModeles(): Observable<Modele[]> {
    return this.http.get<Modele[]>(`${API_URL}/articles/modeles`);
  }

  getCriteres(process: Process): Observable<Critere[]> {
    console.log(process);

    return this.http.get<Critere[]>(
      `${API_URL}/criteres/process/${process.processId}`
    );
  }

  getTypesTris = (): Observable<TypeDeTri[]> =>
    this.http.get<TypeDeTri[]>(`${API_URL}/tris/typestris`);

  getObjectifAnnuel = (): Observable<ObjectifAnnuel> =>
    this.http.get<ObjectifAnnuel>(`${API_URL}/audits/objectif`);

  getAudits = (): Observable<Audit[]> =>
    this.http.get<Audit[]>(`${API_URL}/audits`);

  getAudit = (id: number): Observable<Audit> =>
    this.http.get<Audit>(`${API_URL}/audits/${id}`);

  getCriteresDeTri = (): Observable<CritereDeTri[]> =>
    this.http.get<CritereDeTri[]>(`${API_URL}/tris/criteres`);

  /* -------------------------------------------------------------------------- */
  /*                                    POSTS                                   */

  /* -------------------------------------------------------------------------- */

  postAuditToSend(auditToSend: AuditToSend): any {
    const toSend = JSON.stringify(auditToSend);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };

    console.log('form send');
    return this.http
      .post<AuditToSend>(`${API_URL}/audits`, toSend, {
        headers: httpOptions.headers,
      })
      .subscribe({ error: (error) => console.log(error) });
  }

  postTriToSend(triToSend: TriToSend) {
    const toSend = JSON.stringify(triToSend);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };

    return this.http
      .post<AuditToSend>(`${API_URL}/tris`, toSend, {
        headers: httpOptions.headers,
      })
      .subscribe({ error: (error) => console.log(error) });
  }
}

/* -------------------------------------------------------------------------- */
/*                                 Models Get                                 */
/* -------------------------------------------------------------------------- */

export interface Process {
  processId: number;
  nomProcess: string;
}

export interface Modele {
  modeleId: number;
  modele: string;
}

export interface Article {
  articleId: number;
  codeArticle: number;
  modeleId: number;
  modele: string;
  description: string;
  teinte: string;
  partNumber: number;
}

export interface CodeArticle {
  articleId: number;
  codeArticle: number;
}

export interface Critere {
  critereId: number;
  FK_critereId: number;
  type?: string;
  nomCritere?: string;
  typeObservation?: string;
  valueCritere?: string | any[];
  infoDemerite?: string;
  nbOfDemerites?: Map<number, number>;
}

export interface TypeDeTri {
  typeTriId: number;
  typeTriNom: string;
}

export interface ObjectifAnnuel {
  objectifId: number;
  objectif: number;
  actif: boolean;
}

export interface Audit {
  auditId: number;
  four: string;
  numContainer: number;
  totalVerres: number;
  pourcentageResultat: number;
  rating: string;
  commentaireGeneral?: any;
  action: string;
  dateDebut: Date;
  dateFin: Date;
  fk_objectifAnnuel: number;
  objectif: ObjectifAnnuel;
  fk_user: number;
  fk_article: number;
  article: Article;
  criteres: Critere[];
}

export interface CritereDeTri {
  critereId: number;
  nomCritere: string;
  typeObservation?: any;
  infoDemerite?: any;
  FK_TypeCriteres: number;
  valueCritere: string;
}

/* -------------------------------------------------------------------------- */
/*                                 Models Post                                */
/* -------------------------------------------------------------------------- */

export interface AuditToSend {
  fk_user: number;
  fk_articles: number;
  four: string;
  numContainer: number;
  totalVerres: number;
  resultPourcentage: number;
  objectifAnnuel: number;
  rating: string;
  commentaireGeneral?: string;
  dateDeb: Date;
  action: string;
  //dateFin: string;
  criteres: any;
}

export interface TriToSend {
  fk_user: number;
  fk_typeTris: number;
  fk_market: number;
  fk_LieuAVO?: number;
  fk_articles: number;
  numGallia: number;
  nbPieces: number;
  numOS: number;
  commentaire: string;
  dateDeb: Date;
  dateFin: Date;
  criteres: Critere[];
}
