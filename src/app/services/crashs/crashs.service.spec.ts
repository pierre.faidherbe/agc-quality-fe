import { TestBed } from '@angular/core/testing';

import { CrashsService } from './crashs.service';

describe('CrashsService', () => {
  let service: CrashsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrashsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
