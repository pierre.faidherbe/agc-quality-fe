import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Article } from 'src/app/db.service';
import { Observable } from 'rxjs';
import { Status } from '../articles/articles.service';
import { LigneToSend } from 'src/app/views/admin/crashQualite/lignes-post/lignes-post.component';
import { API_URL } from '../../app.globalConstante';

@Injectable({
  providedIn: 'root',
})
export class CrashsService {
  private options = {
    headers: new HttpHeaders().set('Content-Type', 'application/json'),
  };

  constructor(private http: HttpClient) {}

  getCrash = (id: number) =>
    this.http.get<Crash>(`${API_URL}/crashQualites/${id}`, this.options);

  getCrashs = () =>
    this.http.get<Crash[]>(`${API_URL}/crashQualites`, this.options);

  getAnalysesCrash = () =>
    this.http.get<AnalyseCrash[]>(
      `${API_URL}/crashQualites/analysesCrash`,
      this.options
    );
  getAnalyseCrash = (id: number) =>
    this.http.get<AnalyseCrash>(
      `${API_URL}/crashQualites/analysesCrash/${id}`,
      this.options
    );

  getDecisions = () =>
    this.http.get<Decision[]>(
      `${API_URL}/crashQualites/analysesCrash/decisions`,
      this.options
    );

  getZones = () => {
    return this.http.get<Zone[]>(
      `${API_URL}/crashQualites/zones`,
      this.options
    );
  };

  getMachinesLignesRelation(): Observable<LignesMachines[]> {
    return this.http.get<LignesMachines[]>(
      `${API_URL}/crashQualites/zones/lignes/machines/relations`,
      this.options
    );
  }

  getLignes = () => {
    return this.http.get<Ligne[]>(
      `${API_URL}/crashQualites/zones/lignes`,
      this.options
    );
  };

  getLignesByZone = (id: number) => {
    return this.http.get<Ligne[]>(
      `${API_URL}/crashQualites/zones/${id}/lignes`,
      this.options
    );
  };

  /***
   * function use to get a ligne by her id
   */
  getLigneById(id: number): Observable<Ligne> {
    return this.http.get<Ligne>(
      `${API_URL}/crashQualites/zones/ligne/${id}`,
      this.options
    );
  }

  /***
   * function use to get a zone by her id
   */
  getZoneById(id: number): Observable<Zone> {
    return this.http.get<Zone>(
      `${API_URL}/crashQualites/zone/${id}`,
      this.options
    );
  }

  /***
   * function use to get a zone by her id
   */
  getMachineById(id: number): Observable<MachineToSend> {
    return this.http.get<MachineToSend>(
      `${API_URL}/crashQualites/zones/lignes/machine/${id}`,
      this.options
    );
  }

  getMachines = () => {
    return this.http.get<SousMachine[]>(
      `${API_URL}/crashQualites/zones/lignes/machines`,
      this.options
    );
  };

  getMachinesByLigne = (id: number) => {
    return this.http.get<SousMachine[]>(
      `${API_URL}/crashQualites/zones/lignes/${id}/machines`,
      this.options
    );
  };

  postCrashQualite = (crashToSend: CrashToSend) => {
    const jsonCrashToSend = JSON.stringify(crashToSend);
    console.log(`sending ${jsonCrashToSend}`);

    return this.http
      .post<CrashToSend>(
        `${API_URL}/crashQualites`,
        jsonCrashToSend,
        this.options
      )
      .subscribe({ error: (err) => console.log(err) });
  };

  postAnalyseCrashQualite = (crashToSend: AnalyseCrashToSend) => {
    const jsonCrashToSend = JSON.stringify(crashToSend);
    console.log(`sending ${jsonCrashToSend}`);

    return this.http
      .post<CrashToSend>(
        `${API_URL}/crashQualites/analyseCrash`,
        jsonCrashToSend,
        this.options
      )
      .subscribe({ error: (err) => console.log(err) });
  };

  /***
   * function use to post a new zone
   */
  createZone(zone: Zone | undefined): Observable<Zone> {
    return this.http.post<Zone>(`${API_URL}/crashQualites/zones`, zone);
  }

  /***
   * function use to update a zone
   */
  updateZone(zone: Zone | undefined): Observable<Zone> {
    return this.http.put<Zone>(`${API_URL}/crashQualites/zones`, zone);
  }

  /***
   * function use to delete an zone
   */
  deleteZone(status: Status): Observable<Status> {
    let deleteOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
      body: status,
    };
    return this.http.delete<Status>(
      `${API_URL}/crashQualites/zones`,
      deleteOptions
    );
  }

  /***
   * function use to post a new ligne
   */
  createLigne(ligne: LigneToSend | undefined): Observable<Ligne> {
    return this.http.post<Ligne>(
      `${API_URL}/crashQualites/zones/lignes`,
      ligne
    );
  }

  /***
   * function use to delete a ligne
   */
  deleteLigne(status: Status): Observable<Status> {
    let deleteOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
      body: status,
    };
    return this.http.delete<Status>(
      `${API_URL}/crashQualites/zones/lignes`,
      deleteOptions
    );
  }

  /***
   * function use to update a ligne
   */
  updateLigne(ligne: LigneToSend | undefined): Observable<Ligne> {
    return this.http.put<Ligne>(`${API_URL}/crashQualites/zones/lignes`, ligne);
  }

  /***
   * function use to post a new machine
   */
  createMachine(machine: MachineToSend | undefined): Observable<MachineToSend> {
    return this.http.post<MachineToSend>(
      `${API_URL}/crashQualites/zones/lignes/machines`,
      machine
    );
  }

  /***
   * function use to post a new machine
   */
  updateMachine(machine: MachineToSend | undefined): Observable<MachineToSend> {
    return this.http.put<MachineToSend>(
      `${API_URL}/crashQualites/zones/lignes/machines`,
      machine
    );
  }

  /***
   * function use to delete an machine
   */
  deleteMachine(status: Status): Observable<Status> {
    let deleteOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
      body: status,
    };
    return this.http.delete<Status>(
      `${API_URL}/crashQualites/zones/lignes/machines`,
      deleteOptions
    );
  }
}

export interface SousMachine {
  machineId: number;
  obsolete: boolean;
  machine: string;
  fk_ligne: number;
}

export interface MachineToSend {
  machineId: number | null;
  machine: string;
  listLignes: number[] | null;
}

export interface Ligne {
  ligneId: number;
  ligne: string;
  fk_zone: number | null;
}

export interface Zone {
  zoneId: number | null;
  zone: string;
}

export interface Decision {
  decisionId: number;
  decision: string;
}

export interface DetectionCrash {
  fk_ligne: number;
  fk_machine: number | null;
}

export interface OrigineCrash {
  ligneId: number;
  ligne: string;
  machineId?: number;
  machine?: string;
}

export interface CrashToSend {
  nbPieces: number;
  description: string;
  piecesJointes?: any;
  dateCrash: string;
  fk_user: number;
  fk_article: number;
  detectionCrash: DetectionCrash;
}

export interface AnalyseCrashToSend {
  fk_crashQualite: number;
  fk_decision: number;
  description: string;
  piecesJointes?: any;
  originCrash: {
    fk_ligne: number;
    fk_machine: number | null;
  };
}

export interface Crash {
  crashQualiteId: number;
  nbPieces: number;
  detectionCrash: {
    ligneId: number;
    machineId: number;
    ligne: string;
    machine: string;
  };
  description: string;
  piecesJointes?: any;
  dateCrash: Date;
  statut: boolean;
  fk_user: number;
  fk_article: number;
  article: Article;
}

export interface LignesMachines {
  fk_ligne: number;
  fk_machine: number;
}

export interface AnalyseCrash {
  analyseCrashId: number;
  decision: string;
  description: string;
  piecesJointes?: any;
  fk_crashQualite: number;
  origineCrash: OrigineCrash;
}
