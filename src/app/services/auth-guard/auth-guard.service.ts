import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate {
  constructor(public auth: AuthenticationService, public router: Router) {}

  async canActivate(): Promise<boolean> {
    
    let canActivate = false;  
    await this.auth
      .getCurrentUser()
      .toPromise()
      .then((user) => {
        //User loggué
        if (user != undefined) canActivate = true;
      });

    //Pas d'user loggué -> redirection vers le login
    if (!canActivate) this.router.navigate(['login']);

    return canActivate;
  }
}
