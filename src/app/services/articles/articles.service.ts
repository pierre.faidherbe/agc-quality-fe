import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {API_URL} from "../../app.globalConstante";

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {
  private options = {headers: new HttpHeaders().set('Content-Type', 'application/json')};

  constructor(private http: HttpClient) {
  }

  /***
   * function use to get all articles
   */
  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(`${API_URL}/articles`, this.options);
  }

  /***
   * function use to get an article by his code
   */
  getArticle(id: number): Observable<Article> {
    return this.http.get<Article>(`${API_URL}/articles/${id}`, this.options);
  }

  /***
   * function use to get all article's modele
   */
  getModeles(): Observable<Modele[]> {
    return this.http.get<Modele[]>(`${API_URL}/articles/modeles`, this.options);
  }

  /***
   * function use to get all article's modele
   */
  getModele(id: number): Observable<Modele> {
    return this.http.get<Modele>(`${API_URL}/articles/modeles/${id}`, this.options);
  }


  /***
   * function use to post a new modele
   */
  createModele(modele: Modele | undefined): Observable<Modele> {
    return this.http.post<Modele>(`${API_URL}/articles/modeles`, modele);
  }

  /***
   * function use to post a new modele
   */
  updateModele(modele: Modele | undefined): Observable<Modele> {
    return this.http.put<Modele>(`${API_URL}/articles/modeles`, modele);
  }


  /***
   * function use to delete an modele
   */
  deleteModele(status: Status): Observable<Status> {
    let deleteOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
      body: status
    };
    return this.http.delete<Status>(`${API_URL}/articles/modeles`, deleteOptions);
  }

  /***
   * function use to post a new article
   */
  createArticle(article: Article | undefined): Observable<Article> {
    return this.http.post<Article>(`${API_URL}/articles`, article);
  }

  /***
   * function use to delete an article
   */
  deleteArticle(status: Status): Observable<Status> {
    let deleteOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
      body: status
    };
    return this.http.delete<Status>(`${API_URL}/articles`, deleteOptions);
  }

  /***
   * function use to update an article
   */
  updateArticle(article: Article | undefined): Observable<Article> {
    return this.http.put<Article>(`${API_URL}/articles`, article);
  }
}

export interface Status {
  obsolete: boolean;
  id: number;
}

export interface Article {
  articleId: number | null;
  codeArticle: number | null;
  fk_model: number | null;
  descriptionSAP: string;
  partNumber: string;
}

export interface Modele {
  modeleId: number | null;
  modele: string;
}
