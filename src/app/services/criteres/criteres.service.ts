import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Status} from '../articles/articles.service';
import {API_URL} from "../../app.globalConstante";

@Injectable({
  providedIn: 'root'
})
export class CriteresService {
  private options = {headers: new HttpHeaders().set('Content-Type', 'application/json')};

  constructor(private http: HttpClient) {
  }

  /***
   * function use to get all criteres
   */
  getCriteres(): Observable<Criteres[]> {
    return this.http.get<Criteres[]>(`${API_URL}/criteres`, this.options);
  }

  /***
   * function use to get a critere by id
   */
  getCritereById(id: number): Observable<Criteres> {
    return this.http.get<Criteres>(`${API_URL}/criteres/id/${id}`, this.options);
  }

  /***
   * function use to get all critere's type
   */
  getTypeCritere(): Observable<TypeCritere[]> {
    return this.http.get<TypeCritere[]>(`${API_URL}/criteres/typeCriteres`, this.options);
  }

  /***
   * function use to get a critere's type by id
   */
  getTypeCritereById(id: number): Observable<TypeCritere> {
    return this.http.get<TypeCritere>(`${API_URL}/criteres/typeCritere/${id}`, this.options);
  }

  /***
   * function use to get all process
   */
  getProcess(): Observable<Process[]> {
    return this.http.get<Process[]>(`${API_URL}/criteres/process`, this.options);
  }

  /***
   * function use to get a process by id
   */
  getProcessById(id: number): Observable<Process> {
    return this.http.get<Process>(`${API_URL}/criteres/processById/${id}`, this.options);
  }

  getProcessCriteresRelation(): Observable<ProcessCriteresRel[]> {
    return this.http.get<ProcessCriteresRel[]>(`${API_URL}/criteres/process/relation`, this.options);
  }


  /***
   * function use to post a new type of critere
   */
  createTypeCritere(typeCritere: TypeCritere | undefined): Observable<TypeCritere> {
    return this.http.post<TypeCritere>(`${API_URL}/criteres/typeCriteres`, typeCritere);
  }

  /***
   * function use to update a type of critere
   */
  updateTypeCritere(typeCritere: TypeCritere | undefined): Observable<TypeCritere> {
    return this.http.put<TypeCritere>(`${API_URL}/criteres/typeCriteres`, typeCritere);
  }

  /***
   * function use to delete a type of critere
   */
  deleteTypeCritere(status: Status): Observable<Status> {
    let deleteOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
      body: status
    };
    return this.http.delete<Status>(`${API_URL}/criteres/typeCriteres`, deleteOptions);
  }

  /***
   * function use to post a new process
   */
  createProcess(process: Process | undefined): Observable<Process> {
    return this.http.post<Process>(`${API_URL}/criteres/process`, process);
  }

  /***
   * function use to update a new process
   */
  updateProcess(process: Process | undefined): Observable<Process> {
    return this.http.put<Process>(`${API_URL}/criteres/process`, process);
  }

  /***
   * function use to delete a process
   */
  deleteProcess(status: Status): Observable<Status> {
    let deleteOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
      body: status
    };
    return this.http.delete<Status>(`${API_URL}/criteres/process`, deleteOptions);
  }

  /***
   * function use to post a new critere
   */
  createCritere(critere: CritereToSend | undefined): Observable<CritereToSend> {
    return this.http.post<CritereToSend>(`${API_URL}/criteres`, critere);
  }

  /***
   * function use to update a critere
   */
  updateCritere(critere: CritereToSend | undefined): Observable<CritereToSend> {
    return this.http.put<CritereToSend>(`${API_URL}/criteres`, critere);
  }

  /***
   * function use to delete a critere
   */
  deleteCritere(status: Status): Observable<Status> {
    let deleteOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
      body: status
    };
    return this.http.delete<Status>(`${API_URL}/criteres`, deleteOptions);
  }

}

export interface Criteres {
  critereId: number;
  nomCritere: string;
  typeObservation: string;
  infoDemerite: string;
  FK_TypeCriteres: number;
}

export interface TypeCritere {
  typeCritereId: number | null;
  type: string;
}

export interface Process {
  processId: number | null;
  nomProcess: string;
}

export interface ProcessCriteresRel {
  FK_critereId: number;
  FK_processId: number;
}

export interface CritereToSend {
  critereId: number | null;
  nomCritere: string;
  typeObservation: string;
  infoDemerite: string;
  FK_TypeCriteres: number | null;
  listProcess: number[];
}
