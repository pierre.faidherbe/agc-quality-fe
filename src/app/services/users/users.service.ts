import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Status} from "../articles/articles.service";
import {API_URL} from "../../app.globalConstante";

export interface User {
  idUser: number | null;
  nom: string;
  prenom: string;
  mail: string;
  FK_equipe: number | undefined;
  FK_role: number | undefined;
  BE: number | undefined;
  tracabilite: any;
}

export interface Equipe {
  equipeId: number | null;
  equipe: string;
}

export interface Role {
  roleId: number;
  role: string;
}

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private options = {headers: new HttpHeaders().set('Content-Type', 'application/json')};

  constructor(private http: HttpClient) {
  }

  /***
   * function use to get all users
   */
  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${API_URL}/users`, this.options);
  }

  /***
   * function use to get a user by his id
   */
  getUserById(id: number): Observable<User> {
    return this.http.get<User>(`${API_URL}/users/id/${id}`, this.options);
  }

  /***
   * function use to get all equipe
   */
  getEquipes(): Observable<Equipe[]> {
    return this.http.get<Equipe[]>(`${API_URL}/users/equipes`, this.options);
  }

  /***
   * function use to get an equipe by id
   */
  getEquipeById(id: number): Observable<Equipe> {
    return this.http.get<Equipe>(`${API_URL}/users/equipes/${id}`, this.options);
  }

  /***
   * function use to get all role
   */
  getRoles(): Observable<Role[]> {
    return this.http.get<Role[]>(`${API_URL}/users/roles`, this.options);
  }

  createEquipe(equipe: Equipe | undefined): Observable<Equipe> {
    return this.http.post<Equipe>(`${API_URL}/users/equipe`, equipe);
  }

  updateEquipe(equipe: Equipe | undefined): Observable<Equipe> {
    return this.http.put<Equipe>(`${API_URL}/users/equipe`, equipe);
  }

  /***
   * function use to delete an equipe
   */
  deleteEquipe(status: Status): Observable<Status> {
    let deleteOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
      body: status
    };
    return this.http.delete<Status>(`${API_URL}/users/equipe`, deleteOptions);
  }

  createUser(user: User | undefined): Observable<User> {
    return this.http.post<User>(`${API_URL}/users`, user);
  }

  updateUser(user: User | undefined): Observable<User> {
    return this.http.put<User>(`${API_URL}/users`, user);
  }

  /***
   * function use to delete a user
   */
  deleteUser(status: Status): Observable<Status> {
    let deleteOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
      body: status
    };
    return this.http.delete<Status>(`${API_URL}/users`, deleteOptions);
  }
}
