import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Status} from "../articles/articles.service";
import {API_URL} from "../../app.globalConstante";

@Injectable({
  providedIn: 'root'
})
export class AuditService {
  private options = {headers: new HttpHeaders().set('Content-Type', 'application/json')};

  constructor(private http: HttpClient) {
  }

  /***
   * function use to get all objectifs
   */
  getObjectifs(): Observable<Objectif[]> {
    return this.http.get<Objectif[]>(`${API_URL}/audits/objectif/all`, this.options);
  }

  /***
   * function use to post a new objectif
   */
  createObjectif(objectif: Objectif | undefined): Observable<Objectif> {
    return this.http.post<Objectif>(`${API_URL}/audits/objectif`, objectif);
  }

  /***
   * function use to delete an objectif
   */
  deleteObjectif(status: Status): Observable<Status> {
    let deleteOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
      body: status
    };
    return this.http.delete<Status>(`${API_URL}/audits/objectif`, deleteOptions);
  }

}

export interface Objectif {
  objectifId: number | null;
  objectif: number | null;
  actif: boolean;
}
