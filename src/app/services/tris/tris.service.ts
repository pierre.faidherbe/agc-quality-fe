import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Article, Critere, Modele } from 'src/app/db.service';
import { Status } from '../articles/articles.service';
import {API_URL} from "../../app.globalConstante";

@Injectable({
  providedIn: 'root',
})
export class TrisService {
  private options = {
    headers: new HttpHeaders().set('Content-Type', 'application/json'),
  };

  constructor(private http: HttpClient) {}

  /**Renvoie tous les tris*/
  getTris = () =>
    this.http.get<Tri[]>(`${API_URL}/tris`, this.options);

  /**Renvoie un tri*/
  getTri = (id: Number) =>
    this.http.get<Tri>(`${API_URL}/tris/${id}`, this.options);

  getMarkets = () =>
    this.http.get<Market[]>(`${API_URL}/tris/market`, this.options);

  /***
   * function use to get all tri's type
   */
  getTypeTris(): Observable<TypeTris[]> {
    return this.http.get<TypeTris[]>(
      `${API_URL}/tris/typesTris`,
      this.options
    );
  }

  /***
   * function use to get all tri's type
   */
  getTypeTrisById(id: number): Observable<TypeTris> {
    return this.http.get<TypeTris>(
      `${API_URL}/tris/typesTris/${id}`,
      this.options
    );
  }

  /***
   * function use to get all AVO places
   */
  getAvoLieux(): Observable<Avo[]> {
    return this.http.get<Avo[]>(
      `${API_URL}/tris/typesTris/AVO/Lieux`,
      this.options
    );
  }

  /***
   * function use to get an AVO by id
   */
  getAvoLieuxById(id: number): Observable<Avo> {
    return this.http.get<Avo>(
      `${API_URL}/tris/typesTris/AVO/Lieux/${id}`,
      this.options
    );
  }

  /***
   * function use to post a new type of tri
   */
  createTypeTris(typeTri: TypeTris | undefined): Observable<TypeTris> {
    return this.http.post<TypeTris>(
      `${API_URL}/tris/typesTris`,
      typeTri
    );
  }

  /***
   * function use to post a new type of tri
   */
  updateTypeTris(typeTri: TypeTris | undefined): Observable<TypeTris> {
    return this.http.put<TypeTris>(
      `${API_URL}/tris/typesTris`,
      typeTri
    );
  }

  /***
   * function use to delete a type of tri
   */
  deleteTypeTris(status: Status): Observable<Status> {
    let deleteOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
      body: status,
    };
    return this.http.delete<Status>(
      `${API_URL}/tris/typesTris`,
      deleteOptions
    );
  }

  /***
   * function use to post a new avo
   */
  createAvo(avo: Avo | undefined): Observable<Avo> {
    return this.http.post<Avo>(
      `${API_URL}/tris/typesTris/AVO/Lieux`,
      avo
    );
  }

  /***
   * function use to update an avo
   */
  updateAvo(avo: Avo | undefined): Observable<Avo> {
    return this.http.put<Avo>(
      `${API_URL}/tris/typesTris/AVO/Lieux`,
      avo
    );
  }

  /***
   * function use to delete an AVO
   */
  deleteAVO(status: Status): Observable<Status> {
    let deleteOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
      body: status,
    };
    return this.http.delete<Status>(
      `${API_URL}/tris/typesTris/AVO/Lieux`,
      deleteOptions
    );
  }
}

export interface Tri {
  triId: number;
  numGallia: number;
  numOS: number;
  nbPieces: number;
  commentaireGeneral: string;
  dateDebut: Date;
  dateFin: Date;
  fk_user: number;
  fk_article: number;
  article: Article;
  fk_typeTri: number;
  typeTri: TypeTris;
  fk_LieuAVO?: any;
  avo: Avo;
  fk_market: number;
  market: Market;
  criteres: Critere[];
}

export interface Market {
  marketId: number;
  nom: string;
}

export interface TypeTris {
  typeTriId: number | null;
  typeTriNom: string;
}

export interface Avo {
  lieuAvoId: number | null;
  nomLieu: string;
  fk_typeTri_AVO: number;
}
