import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TableModule } from 'primeng/table';
import { EnregistrementAuditComponent } from './views/enregistrement-audit/enregistrement-audit.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TopFormAuditComponent } from './components/top-form-audit/top-form-audit.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { BottomFormComponent } from './components/bottom-form/bottom-form.component';
import { HttpClientModule } from '@angular/common/http';
import { InputTextModule } from 'primeng/inputtext';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { AnalyseFormAuditComponent } from './components/analyse-form-audit/analyse-form-audit.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTooltipModule } from '@angular/material/tooltip';
import { DividerModule } from 'primeng/divider';
import { TextFieldModule } from '@angular/cdk/text-field';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatRadioModule } from '@angular/material/radio';
import { TriAuditComponent } from './views/tri-audit/tri-audit.component';
import { TopFormTriComponent } from './components/top-form-tri/top-form-tri.component';
import { CriteresFormTriComponent } from './components/criteres-form-tri/criteres-form-tri.component';
import { CartoTriComponent } from './components/carto-tri/carto-tri.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatRippleModule } from '@angular/material/core';
import { LoginComponent } from './views/login/login.component';
import { DropdownModule } from 'primeng/dropdown';
import { UsersListingComponent } from './views/admin/users/users-listing/users-listing.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { HomeComponent } from './views/home/home.component';
import { EquipesListingComponent } from './views/admin/users/equipes-listing/equipes-listing.component';
import { ArticlesListingComponent } from './views/admin/articles/articles-listing/articles-listing.component';
import { ModelesListingComponent } from './views/admin/articles/modeles-listing/modeles-listing.component';
import { CriteresListingComponent } from './views/admin/criteres/criteres-listing/criteres-listing.component';
import { TypesCriteresListingComponent } from './views/admin/criteres/types-criteres-listing/types-criteres-listing.component';
import { ProcessListingComponent } from './views/admin/criteres/process-listing/process-listing.component';
import { MatChipsModule } from '@angular/material/chips';
import { CrashQualiteComponent } from './views/crash-qualite/crash-qualite.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatMenuModule } from '@angular/material/menu';
import { DialogZoneComponent } from './components/Dialogs/dialog-zone/dialog-zone.component';
import { MatStepperModule } from '@angular/material/stepper';
import { ObjectifAnnuelListingComponent } from './views/admin/audit/objectif-annuel-listing/objectif-annuel-listing.component';
import { TypesTrisListingComponent } from './views/admin/tris/types-tris-listing/types-tris-listing.component';
import { AvoListingComponent } from './views/admin/tris/avo-listing/avo-listing.component';
import { PostEquipeComponent } from './views/admin/users/post-equipe/post-equipe.component';
import { PostUsersComponent } from './views/admin/users/post-users/post-users.component';
import { TypesTrisPostComponent } from './views/admin/tris/types-tris-post/types-tris-post.component';
import { AvoPostComponent } from './views/admin/tris/avo-post/avo-post.component';
import { TypesCriteresPostComponent } from './views/admin/criteres/types-criteres-post/types-criteres-post.component';
import { ProcessPostComponent } from './views/admin/criteres/process-post/process-post.component';
import { CriteresPostComponent } from './views/admin/criteres/criteres-post/criteres-post.component';
import { ObjectifAnnuelPostComponent } from './views/admin/audit/objectif-annuel-post/objectif-annuel-post.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ModelesPostComponent } from './views/admin/articles/modeles-post/modeles-post.component';
import { ArticlesPostComponent } from './views/admin/articles/articles-post/articles-post.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ChooseAuditDialogComponent } from './components/Dialogs/choose-audit-dialog/choose-audit-dialog.component';
import { registerLocaleData } from '@angular/common';
import localeFRBE from '@angular/common/locales/fr-BE';
import { ChooseCrashDialogComponent } from './components/Dialogs/choose-crash-dialog/choose-crash-dialog.component';
import { OpenCrashQualiteComponent } from './views/open-crash-qualite/open-crash-qualite.component';
import { ChooseTriDialogComponent } from './components/Dialogs/choose-tri-dialog/choose-tri-dialog.component';
import { ZonesComponent } from './views/admin/crashQualite/zones/zones.component';
import { LignesComponent } from './views/admin/crashQualite/lignes/lignes.component';
import { MachinesComponent } from './views/admin/crashQualite/machines/machines.component';
import { ZonesPostComponent } from './views/admin/crashQualite/zones-post/zones-post.component';
import { LignesPostComponent } from './views/admin/crashQualite/lignes-post/lignes-post.component';
import { MachinesPostComponent } from './views/admin/crashQualite/machines-post/machines-post.component';
import { ModelesUpdateComponent } from './views/admin/articles/modeles-update/modeles-update.component';
import { ArticlesUpdateComponent } from './views/admin/articles/articles-update/articles-update.component';
import { ObjectifUpdateComponent } from './views/admin/audit/objectif-update/objectif-update.component';
import { LignesUpdateComponent } from './views/admin/crashQualite/lignes-update/lignes-update.component';
import { ZonesUpdateComponent } from './views/admin/crashQualite/zones-update/zones-update.component';
import { MachinesUpdateComponent } from './views/admin/crashQualite/machines-update/machines-update.component';
import { TypesCriteresUpdateComponent } from './views/admin/criteres/types-criteres-update/types-criteres-update.component';
import { ProcessUpdateComponent } from './views/admin/criteres/process-update/process-update.component';
import { CriteresUpdateComponent } from './views/admin/criteres/criteres-update/criteres-update.component';
import { AvoUpdateComponent } from './views/admin/tris/avo-update/avo-update.component';
import { TypesTrisUpdateComponent } from './views/admin/tris/types-tris-update/types-tris-update.component';
import { EquipesUpdateComponent } from './views/admin/users/equipes-update/equipes-update.component';
import { UsersUpdateComponent } from './views/admin/users/users-update/users-update.component';
import { ClosedCrashQualiteComponent } from './views/closed-crash-qualite/closed-crash-qualite.component';
import {ToastrModule} from "ngx-toastr";
registerLocaleData(localeFRBE);

@NgModule({
  declarations: [
    AppComponent,
    EnregistrementAuditComponent,
    TopFormAuditComponent,
    TopBarComponent,
    BottomFormComponent,
    AnalyseFormAuditComponent,
    TriAuditComponent,
    TopFormTriComponent,
    CriteresFormTriComponent,
    CartoTriComponent,
    LoginComponent,
    HomeComponent,
    UsersListingComponent,
    EquipesListingComponent,
    ArticlesListingComponent,
    ModelesListingComponent,
    CriteresListingComponent,
    TypesCriteresListingComponent,
    ProcessListingComponent,
    CrashQualiteComponent,
    DialogZoneComponent,
    ObjectifAnnuelListingComponent,
    TypesTrisListingComponent,
    AvoListingComponent,
    PostEquipeComponent,
    PostUsersComponent,
    TypesTrisPostComponent,
    AvoPostComponent,
    TypesCriteresPostComponent,
    ProcessPostComponent,
    CriteresPostComponent,
    ObjectifAnnuelPostComponent,
    ModelesPostComponent,
    ArticlesPostComponent,
    ChooseAuditDialogComponent,
    ChooseCrashDialogComponent,
    OpenCrashQualiteComponent,
    ChooseTriDialogComponent,
    ZonesComponent,
    LignesComponent,
    MachinesComponent,
    ZonesPostComponent,
    LignesPostComponent,
    MachinesPostComponent,
    ModelesUpdateComponent,
    ArticlesUpdateComponent,
    ObjectifUpdateComponent,
    LignesUpdateComponent,
    ZonesUpdateComponent,
    MachinesUpdateComponent,
    TypesCriteresUpdateComponent,
    ProcessUpdateComponent,
    CriteresUpdateComponent,
    AvoUpdateComponent,
    TypesTrisUpdateComponent,
    EquipesUpdateComponent,
    UsersUpdateComponent,
    ClosedCrashQualiteComponent,
  ],
  imports: [
    MatChipsModule,
    BrowserModule,
    NgxDatatableModule,
    MatStepperModule,
    DropdownModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatListModule,
    MatNativeDateModule,
    MatDatepickerModule,
    HttpClientModule,
    MatMenuModule,
    MatRippleModule,
    MatRadioModule,
    MatDialogModule,
    DividerModule,
    MatSidenavModule,
    AppRoutingModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatInputModule,
    TextFieldModule,
    MatProgressSpinnerModule,
    TableModule,
    MatCheckboxModule,
    InputTextModule,
    MatIconModule,
    MatButtonModule,
    MatProgressBarModule,
    MatCardModule,
    BrowserAnimationsModule,
    MatSelectModule,
    ReactiveFormsModule,
    FormsModule,
    MatSlideToggleModule,
    ToastrModule.forRoot()
  ],
  providers: [DatePipe, { provide: LOCALE_ID, useValue: 'fr-BE' }],
  bootstrap: [AppComponent],
})
export class AppModule {}
