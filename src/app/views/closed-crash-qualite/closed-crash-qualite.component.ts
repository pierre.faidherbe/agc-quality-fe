import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  AnalyseCrash,
  Crash,
  CrashsService,
} from 'src/app/services/crashs/crashs.service';

@Component({
  selector: 'app-closed-crash-qualite',
  templateUrl: './closed-crash-qualite.component.html',
  styleUrls: ['./closed-crash-qualite.component.css'],
})
export class ClosedCrashQualiteComponent implements OnInit {
  crashReceived?: Crash;
  analyseReceived?: AnalyseCrash;

  constructor(
    private activatedRoute: ActivatedRoute,
    private crashsService: CrashsService
  ) {}

  ngOnInit(): void {
    const id = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    this.crashsService.getCrash(id).subscribe(
      (crash: Crash) => (this.crashReceived = crash),
      (err) => console.log(err),
      () => {
        this.crashsService
          .getAnalyseCrash(this.crashReceived?.crashQualiteId!)
          .subscribe((analyse) => (this.analyseReceived = analyse));
      }
    );
  }
}
