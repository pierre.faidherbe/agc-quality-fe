import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogZoneComponent } from 'src/app/components/Dialogs/dialog-zone/dialog-zone.component';
import {
  AnalyseCrashToSend,
  Crash,
  CrashsService,
  Decision,
  Ligne,
  SousMachine,
} from 'src/app/services/crashs/crashs.service';

@Component({
  selector: 'app-open-crash-qualite',
  templateUrl: './open-crash-qualite.component.html',
  styleUrls: ['./open-crash-qualite.component.css'],
})
export class OpenCrashQualiteComponent implements OnInit {
  crashReceived?: Crash;

  formGroup: FormGroup = new FormGroup({});
  selectedLigne?: Ligne;
  selectedMachine?: SousMachine;
  decisions: Decision[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private crashsService: CrashsService,
    private matDialog: MatDialog,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    const id = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    this.crashsService
      .getCrash(id)
      .subscribe((crash: Crash) => (this.crashReceived = crash));

    this.crashsService
      .getDecisions()
      .subscribe((decisions) => (this.decisions = decisions));

    this.buildFormulaire();
  }

  buildFormulaire() {
    this.formGroup = this.formBuilder.group({
      lieuDorigine: [],
      description: [],
      decision: [],
    });
  }

  onClickToOpenDialog() {
    const ref = this.matDialog.open(DialogZoneComponent, {});
    ref.afterClosed().subscribe((data) => {
      this.selectedLigne = data[0];
      this.selectedMachine = data[1];
      this.formGroup.controls['lieuDorigine'].setValue(data);
    });
  }

  getLigneOrMachineValue = (ligneOrMachine: any[]) => {
    return ligneOrMachine != undefined
      ? (ligneOrMachine[1] as SousMachine).machine ||
          (ligneOrMachine[0] as Ligne).ligne
      : undefined;
  };

  submitAnalyse = () => {
    const analyseToSend: AnalyseCrashToSend = {
      fk_decision: this.formGroup.controls['decision'].value['decisionId'],
      description: this.formGroup.controls['description'].value,
      originCrash: {
        fk_ligne: this.selectedLigne!.ligneId,
        fk_machine: this.selectedMachine!.machineId || null,
      },
      fk_crashQualite: this.crashReceived!.crashQualiteId,
      piecesJointes: null, //TODO
    };

    console.log(analyseToSend);
    this.crashsService.postAnalyseCrashQualite(analyseToSend);

    window.alert('Analyse crash envoyée');
    this.router.navigateByUrl('/home');
  };
}
