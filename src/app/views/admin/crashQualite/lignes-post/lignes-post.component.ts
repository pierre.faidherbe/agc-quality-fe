import {Component, OnInit} from '@angular/core';
import {CrashsService, Zone} from '../../../../services/crashs/crashs.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-lignes-post',
  templateUrl: './lignes-post.component.html',
  styleUrls: ['./lignes-post.component.css']
})
export class LignesPostComponent implements OnInit {
  // @ts-ignore
  newLigne: LigneToSend;
  zonesList: Zone[] = [];

  constructor(
    private crashService: CrashsService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.newLigne = new LigneToSend(null, '', null);
    this.getZone();
  }

  getZone(): void {
    this.crashService.getZones().subscribe(res => {
      this.zonesList = res;
    });
  }

  save(): void {
    this.crashService.createLigne(this.newLigne).subscribe(res => {
      console.log(res);
    }, error => {
      console.log(error);
    }, () => {
    });
    this.alertNotif();
  }

  alertNotif(): void {
    this.toastr.success('La ligne a bien été enregistrée.', 'Enregistrement', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }

}

export class LigneToSend {
  constructor(
    public ligneId: number | null,
    public  ligne: string,
    // tslint:disable-next-line:variable-name
    public fk_zone: number | null,
  ) {
  }
}
