import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LignesPostComponent } from './lignes-post.component';

describe('LignesPostComponent', () => {
  let component: LignesPostComponent;
  let fixture: ComponentFixture<LignesPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LignesPostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LignesPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
