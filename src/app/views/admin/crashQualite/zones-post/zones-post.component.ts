import {Component, OnInit} from '@angular/core';
import {CrashsService} from "../../../../services/crashs/crashs.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-zones-post',
  templateUrl: './zones-post.component.html',
  styleUrls: ['./zones-post.component.css']
})
export class ZonesPostComponent implements OnInit {
  // @ts-ignore
  newZone: ZoneToSend;

  constructor(
    private crashService: CrashsService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.newZone = new ZoneToSend('', null);
  }

  save(): void {
    this.crashService.createZone(this.newZone).subscribe(res => {
      console.log(res);
    }, error => {
      console.log(error);
    }, () => {
    });
    this.alertNotif();
  }

  alertNotif(): void {
    this.toastr.success('La zone a bien été enregistrée.', 'Enregistrement', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }

}

export class ZoneToSend {
  constructor(
    public zone: string,
    public zoneId: number | null,
  ) {
  }
}
