import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZonesPostComponent } from './zones-post.component';

describe('ZonesPostComponent', () => {
  let component: ZonesPostComponent;
  let fixture: ComponentFixture<ZonesPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZonesPostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZonesPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
