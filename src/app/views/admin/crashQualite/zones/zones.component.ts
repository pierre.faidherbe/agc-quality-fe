import {Component, OnInit} from '@angular/core';
import {MAT_TOOLTIP_DEFAULT_OPTIONS, MatTooltipDefaultOptions} from '@angular/material/tooltip';
import {CrashsService, Zone} from '../../../../services/crashs/crashs.service';
import {ToastrService} from "ngx-toastr";

export const myCustomTooltipDefaults: MatTooltipDefaultOptions = {
  showDelay: 1000,
  hideDelay: 1000,
  touchendHideDelay: 1000,
};

@Component({
  selector: 'app-zones',
  templateUrl: './zones.component.html',
  styleUrls: ['./zones.component.css'],
  providers: [
    {provide: MAT_TOOLTIP_DEFAULT_OPTIONS, useValue: myCustomTooltipDefaults}
  ],
})
export class ZonesComponent implements OnInit {
  Zones: ZoneView[] = [];
  data: Zone[] = [];
  temp: ZoneView[] = [];

  constructor(
    private crashService: CrashsService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.getZones();
  }

  getZones(): void {
    this.crashService.getZones().subscribe(res => {
        this.data = res;
      }, error => {
      },
      () => {
        for (let entry of this.data) {
          this.Zones.push({
            zone: entry.zone,
            zoneId: entry.zoneId,
            // @ts-ignore
            obsolete: entry.obsolete
          });
        }
        this.Zones = [...this.Zones];
        this.temp = this.Zones;
      });
  }

  filterData(event: any): void {
    const val = event.target.value.toLowerCase();
    // tslint:disable-next-line:only-arrow-functions
    const temp = this.temp.filter(function (d) {
      return d.zone.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.Zones = temp;
  }

  changeStatut(obsolete: boolean, id: number): void {
    let jsonToSend = {obsolete, id};
    this.crashService.deleteZone(jsonToSend).subscribe(res => {
        this.Zones.length = 0;
        this.getZones();
      }, error => {
      },
      () => {
        this.alertNotif(obsolete);
      });
  }

  alertNotif(obsolete: boolean): void {
    if (obsolete) {
      this.toastr.success('La zone a bien été désactivée.', 'Désactivation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    } else {
      this.toastr.success('La zone a bien été activée.', 'Activation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    }
  }

}

export interface ZoneView {
  zoneId: number | null;
  obsolete: boolean;
  zone: string;
}
