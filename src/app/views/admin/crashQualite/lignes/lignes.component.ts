import {Component, OnInit} from '@angular/core';
import {MAT_TOOLTIP_DEFAULT_OPTIONS, MatTooltipDefaultOptions} from '@angular/material/tooltip';
import {CrashsService, Ligne, Zone} from '../../../../services/crashs/crashs.service';
import {ToastrService} from "ngx-toastr";

export const myCustomTooltipDefaults: MatTooltipDefaultOptions = {
  showDelay: 1000,
  hideDelay: 1000,
  touchendHideDelay: 1000,
};

@Component({
  selector: 'app-lignes',
  templateUrl: './lignes.component.html',
  styleUrls: ['./lignes.component.css'],
  providers: [
    {provide: MAT_TOOLTIP_DEFAULT_OPTIONS, useValue: myCustomTooltipDefaults}
  ],
})
export class LignesComponent implements OnInit {
  Zones: Zone[] = [];
  Lignes: Ligne[] = [];
  temp: LigneView[] = [];
  LignesToView: LigneView[] = [];

  constructor(
    private crashService: CrashsService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.getZones();
  }

  getZones(): void {
    this.crashService.getZones().subscribe(res => {
      this.Zones = res;
    }, error => {
      console.log(error);
    }, () => {
      this.getLignes();
    });
  }

  getLignes(): void {
    this.crashService.getLignes().subscribe(res => {
      this.Lignes = res;
      for (let ligne of this.Lignes) {
        let ligneZone: string;
        for (let zone of this.Zones) {
          if (ligne.fk_zone === zone.zoneId) {
            ligneZone = zone.zone;
          }
        }
        this.LignesToView.push({
          ligne: ligne.ligne,
          // @ts-ignore
          ligneId: ligne.ligneId,
          // @ts-ignore
          ligneObsolete: ligne.obsolete,
          // @ts-ignore
          fk_zone: ligne.fk_zone,
          // @ts-ignore
          zone: ligneZone
        });
      }
      this.LignesToView = [...this.LignesToView];
      this.temp = this.LignesToView;
    });
  }

  filterData(event: any): void {
    const val = event.target.value.toLowerCase();
    // tslint:disable-next-line:only-arrow-functions
    const temp = this.temp.filter(function (d) {
      return d.ligne.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.LignesToView = temp;
  }

  changeStatut(obsolete: boolean, id: number): void {
    let jsonToSend = {obsolete, id};
    this.crashService.deleteLigne(jsonToSend).subscribe(res => {
        this.LignesToView.length = 0;
        this.getLignes();
      }, error => {
      },
      () => {
        this.alertNotif(obsolete)
      });
  }


  alertNotif(obsolete: boolean): void {
    if (obsolete) {
      this.toastr.success('La ligne a bien été désactivée.', 'Désactivation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    } else {
      this.toastr.success('La ligne a bien été activée.', 'Activation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    }
  }

}

interface LigneView {
  ligneId: number;
  ligne: string;
  zone: string;
  ligneObsolete: boolean;
  fk_zone: number;
}
