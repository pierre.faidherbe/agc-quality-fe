import {Component, OnInit} from '@angular/core';
import {CrashsService, Ligne, Zone} from '../../../../services/crashs/crashs.service';
import {LigneToSend} from '../lignes-post/lignes-post.component';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-lignes-update',
  templateUrl: './lignes-update.component.html',
  styleUrls: ['./lignes-update.component.css']
})
export class LignesUpdateComponent implements OnInit {
  temp: Ligne | undefined;
  // @ts-ignore
  ligneToUpdate: LigneToSend;
  ligneId: any;
  zonesList: Zone[] = [];

  constructor(
    private crashService: CrashsService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.ligneToUpdate = new LigneToSend(null, '', null);
    this.ligneId = this.route.snapshot.paramMap.get('id');
    this.getZoneList();
    this.getLigne();
  }

  getZoneList(): void {
    this.crashService.getZones().subscribe(res => {
      this.zonesList = res;
    });
  }

  getLigne(): void {
    this.crashService.getLigneById(this.ligneId).subscribe(res => {
        this.temp = res;
      }, error => {
        console.log(error);
      },
      () => {
        // @ts-ignore
        this.ligneToUpdate.ligneId = this.temp.ligneId;
        // @ts-ignore
        this.ligneToUpdate.ligne = this.temp?.ligne;
        // @ts-ignore
        this.ligneToUpdate.fk_zone = this.temp?.fk_zone;
      }
    );
  }

  save(): void {
    this.crashService.updateLigne(this.ligneToUpdate).subscribe(res => {
        console.log(res);
      }, error => {
      },
      () => {
      });
    this.alertNotif()
  }

  alertNotif(): void {
    this.toastr.success('La ligne a bien été modifiée.', 'Update', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }
}
