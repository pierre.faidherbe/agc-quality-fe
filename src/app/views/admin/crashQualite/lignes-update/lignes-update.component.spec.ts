import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LignesUpdateComponent } from './lignes-update.component';

describe('LignesUpdateComponent', () => {
  let component: LignesUpdateComponent;
  let fixture: ComponentFixture<LignesUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LignesUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LignesUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
