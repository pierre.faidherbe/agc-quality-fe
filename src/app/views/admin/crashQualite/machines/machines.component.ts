import {Component, OnInit} from '@angular/core';
import {MAT_TOOLTIP_DEFAULT_OPTIONS, MatTooltipDefaultOptions} from '@angular/material/tooltip';
import {CrashsService, Ligne, LignesMachines, SousMachine} from '../../../../services/crashs/crashs.service';
import {ToastrService} from "ngx-toastr";

export const myCustomTooltipDefaults: MatTooltipDefaultOptions = {
  showDelay: 1000,
  hideDelay: 1000,
  touchendHideDelay: 1000,
};

@Component({
  selector: 'app-machines',
  templateUrl: './machines.component.html',
  styleUrls: ['./machines.component.css'],
  providers: [
    {provide: MAT_TOOLTIP_DEFAULT_OPTIONS, useValue: myCustomTooltipDefaults}
  ],
})
export class MachinesComponent implements OnInit {
  Data: MachinesView[] = [];
  Machines: SousMachine[] = [];
  Lignes: Ligne[] = [];
  LignesMachinesRelation: LignesMachines[] = [];
  temp: MachinesView[] = [];

  constructor(
    private crashService: CrashsService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.getLignesList();
  }

  getLignesList(): void {
    this.crashService.getLignes().subscribe(res => {
        this.Lignes = res;
      }, error => {
      }, () => {
        this.getLignesMachines();
      }
    );
  }

  getLignesMachines(): void {
    this.crashService.getMachinesLignesRelation().subscribe(res => {
        this.LignesMachinesRelation = res;
      }, error => {
      },
      () => {
        this.getMachines();
      });
  }

  getMachines(): void {
    this.crashService.getMachines().subscribe(res => {
        this.Machines = res;
      }, error => {
      },
      () => {
        for (let entry of this.Machines) {
          let listLignes = [];
          for (let ligne of this.LignesMachinesRelation) {
            if (ligne.fk_machine === entry.machineId) {
              for (let ligneToPush of this.Lignes) {
                if (ligne.fk_ligne === ligneToPush.ligneId) {
                  listLignes.push(ligneToPush.ligne);
                }
              }
            }
          }
          this.Data.push({
            machineId: entry.machineId,
            machine: entry.machine,
            obsolete: entry.obsolete,
            lignesList: listLignes
          });
        }
        this.Data = [...this.Data];
        this.temp = this.Data;
      });
  }

  filterData(event: any): void {
    const val = event.target.value.toLowerCase();
    // tslint:disable-next-line:only-arrow-functions
    const temp = this.temp.filter(function (d) {
      return d.machine.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.Data = temp;
  }

  changeStatut(obsolete: boolean, id: number): void {
    let jsonToSend = {obsolete, id};
    this.crashService.deleteMachine(jsonToSend).subscribe(res => {
        this.Data.length = 0;
        this.getMachines();
      }, error => {
      },
      () => {
        this.alertNotif(obsolete);
      });
  }

  alertNotif(obsolete: boolean): void {
    if (obsolete) {
      this.toastr.success('La machine a bien été désactivée.', 'Désactivation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    } else {
      this.toastr.success('La machine a bien été activée.', 'Activation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    }
  }
}

interface MachinesView {
  machineId: number;
  machine: string;
  obsolete: boolean;
  lignesList: string[] | undefined;
}
