import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MachinesPostComponent } from './machines-post.component';

describe('MachinesPostComponent', () => {
  let component: MachinesPostComponent;
  let fixture: ComponentFixture<MachinesPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MachinesPostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MachinesPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
