import {Component, OnInit} from '@angular/core';
import {CrashsService, Ligne} from '../../../../services/crashs/crashs.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-machines-post',
  templateUrl: './machines-post.component.html',
  styleUrls: ['./machines-post.component.css']
})
export class MachinesPostComponent implements OnInit {
  // @ts-ignore
  machineToSend: MachineToSend;
  listLignes: Ligne[] = [];

  constructor(
    private crashService: CrashsService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.machineToSend = new MachineToSend(null, '', null);
    this.getLignes();
  }

  getLignes(): void {
    this.crashService.getLignes().subscribe(res => {
      this.listLignes = res;
    });
  }

  save(): void {
    this.crashService.createMachine(this.machineToSend).subscribe(res => {
        console.log(res);
      }, error => {
        console.log(error);
      },
      () => {
      });
    this.alertNotif();
  }

  alertNotif(): void {
    this.toastr.success('La machine a bien été enregistrée.', 'Enregistrement', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }

}

export class MachineToSend {
  constructor(
    public machineId: number | null,
    public machine: string,
    public listLignes: number[] | null,
  ) {
  }
}
