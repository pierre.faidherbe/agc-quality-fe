import {Component, OnInit} from '@angular/core';
import {CrashsService, Zone} from '../../../../services/crashs/crashs.service';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-zones-update',
  templateUrl: './zones-update.component.html',
  styleUrls: ['./zones-update.component.css']
})
export class ZonesUpdateComponent implements OnInit {
  // @ts-ignore
  zoneToUpdate: Zone;
  zoneId: any;

  constructor(
    private crashService: CrashsService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.zoneId = this.route.snapshot.paramMap.get('id');
    this.getZoneToUpdate();
  }

  getZoneToUpdate(): void {
    this.crashService.getZoneById(this.zoneId).subscribe(res => {
      this.zoneToUpdate = res;
    });
  }

  save(): void {
    this.crashService.updateZone({zone: this.zoneToUpdate.zone, zoneId: this.zoneToUpdate.zoneId}).subscribe(res => {
        console.log(res);
      }, error => {
      },
      () => {
      });
    this.alertNotif();
  }

  alertNotif(): void {
    this.toastr.success('La zone a bien été modifiée.', 'Update', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }
}
