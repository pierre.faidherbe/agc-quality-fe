import {Component, OnInit} from '@angular/core';
import {
  CrashsService,
  Ligne,
  LignesMachines,
  MachineToSend,
  SousMachine
} from '../../../../services/crashs/crashs.service';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-machines-update',
  templateUrl: './machines-update.component.html',
  styleUrls: ['./machines-update.component.css']
})
export class MachinesUpdateComponent implements OnInit {
  // @ts-ignore
  temp: MachineToSend;
  machineId: any;
  // @ts-ignore
  machineLigneRel: LignesMachines[] = [];
  listLignes: Ligne[] = [];

  constructor(
    private crashService: CrashsService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.machineId = this.route.snapshot.paramMap.get('id');
    this.getMachineLignesRel();
  }

  getMachineLignesRel(): void {
    this.crashService.getMachinesLignesRelation().subscribe(res => {
        this.machineLigneRel = res;
      }, error => {
      },
      () => {
        this.getMachine();
        this.getListLignes();
      });
  }

  getListLignes(): void {
    this.crashService.getLignes().subscribe(res => {
      this.listLignes = res;
    });
  }

  getMachine(): void {
    this.crashService.getMachineById(this.machineId).subscribe(res => {
        this.temp = res;
      }, error => {
      },
      () => {
        let tempsListLigne = [];
        for (let relation of this.machineLigneRel) {
          if (this.temp.machineId === relation.fk_machine) {
            tempsListLigne.push(relation.fk_ligne);
          }
        }
        this.temp.listLignes = tempsListLigne;
      });
  }

  save(): void {
    this.crashService.updateMachine(this.temp).subscribe(res => {
        console.log(res);
      }, error => {
      },
      () => {
      });
    this.alertNotif();
  }

  alertNotif(): void {
    this.toastr.success('La machine a bien été modifiée.', 'Update', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }
}
