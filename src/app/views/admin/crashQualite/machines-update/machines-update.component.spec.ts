import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MachinesUpdateComponent } from './machines-update.component';

describe('MachinesUpdateComponent', () => {
  let component: MachinesUpdateComponent;
  let fixture: ComponentFixture<MachinesUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MachinesUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MachinesUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
