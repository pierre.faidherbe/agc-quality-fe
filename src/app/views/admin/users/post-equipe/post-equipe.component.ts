import {Component, OnInit} from '@angular/core';
import {UsersService} from '../../../../services/users/users.service';
import {ToastrService} from "ngx-toastr";

export class Equipe {
  constructor(
    public equipe: string,
    public equipeId: number | null,
  ) {
  }
}

@Component({
  selector: 'app-post-equipe',
  templateUrl: './post-equipe.component.html',
  styleUrls: ['./post-equipe.component.css']
})
export class PostEquipeComponent implements OnInit {
  // @ts-ignore
  newEquipe: Equipe;

  constructor(
    private usersService: UsersService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.newEquipe = new Equipe('', null);
  }

  save(): void {
    this.usersService.createEquipe(this.newEquipe).subscribe(res => {
    }, error => {
      console.log(error);
    }, () => {
    });
    this.alertNotif()
  }

  alertNotif(): void {
    this.toastr.success('L\'equipe a bien été enregistrée.', 'Enregistrement', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }
}

