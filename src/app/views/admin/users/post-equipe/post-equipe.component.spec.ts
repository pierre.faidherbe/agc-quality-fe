import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostEquipeComponent } from './post-equipe.component';

describe('PostEquipeComponent', () => {
  let component: PostEquipeComponent;
  let fixture: ComponentFixture<PostEquipeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostEquipeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostEquipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
