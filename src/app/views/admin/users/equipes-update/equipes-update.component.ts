import {Component, OnInit} from '@angular/core';
import {UsersService} from '../../../../services/users/users.service';
import {ActivatedRoute} from '@angular/router';
import {Equipe} from '../post-equipe/post-equipe.component';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-equipes-update',
  templateUrl: './equipes-update.component.html',
  styleUrls: ['./equipes-update.component.css']
})
export class EquipesUpdateComponent implements OnInit {
  id = 0;
  // @ts-ignore
  equipeToUpdate: Equipe;

  constructor(
    private usersService: UsersService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.equipeToUpdate = new Equipe('', null);
    this.id = parseInt(this.route.snapshot.paramMap.get('id') as string, 10);
    this.getEquipeToUpdate();
  }

  getEquipeToUpdate(): void {
    this.usersService.getEquipeById(this.id).subscribe(res => {
      this.equipeToUpdate = res;
    });
  }

  save(): void {
    this.usersService.updateEquipe(this.equipeToUpdate).subscribe(res => {
        console.log(res);
      }, error => {
      },
      () => {
      });
    this.alertNotif();
  }

  alertNotif(): void {
    this.toastr.success('L\'equipe a bien été modifiée.', 'Update', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }

}
