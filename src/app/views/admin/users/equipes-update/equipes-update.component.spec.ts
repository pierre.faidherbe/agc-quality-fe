import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipesUpdateComponent } from './equipes-update.component';

describe('EquipesUpdateComponent', () => {
  let component: EquipesUpdateComponent;
  let fixture: ComponentFixture<EquipesUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquipesUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipesUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
