import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Equipe, Role, UsersService} from '../../../../services/users/users.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-post-users',
  templateUrl: './post-users.component.html',
  styleUrls: ['./post-users.component.css']
})
export class PostUsersComponent implements OnInit {
  email = new FormControl('', [Validators.required, Validators.email]);
  nom = new FormControl('', [Validators.required, Validators.minLength(3)]);
  prenom = new FormControl('', [Validators.required, Validators.minLength(3)]);
  selectEquipe = new FormControl('', [Validators.required]);
  selectRole = new FormControl('', [Validators.required]);
  equipesList: Equipe[] = [];
  roleList: Role[] = [];

  // @ts-ignore
  userToSend: User;

  constructor(
    private usersService: UsersService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.userToSend = new User(null, '', '', '', undefined, undefined, undefined, null);
    this.getEquipesList();
    this.getRoleList();
  }

  getEquipesList(): void {
    this.usersService.getEquipes().subscribe(res => {
      this.equipesList = res;
    });
  }

  getRoleList(): void {
    this.usersService.getRoles().subscribe(res => {
      this.roleList = res;
    });
  }

  getErrorMessage(): string {
    if (this.email.hasError('required')) {
      return 'Le champs doit être rempli';
    }
    if (this.email.hasError('email')) {
      return 'l\'email n\'est pas valide';
    }
    return '';
  }

  getSelectEquipeErrorMessage(): string {
    if (this.selectEquipe.hasError('required')) {
      return 'Selectionnez une équipe';
    }
    return '';
  }

  getSelectRoleErrorMessage(): string {
    if (this.selectRole.hasError('required')) {
      return 'Selectionnez un role';
    }
    return '';
  }

  getNomErrorMsg(): string {
    if (this.nom.hasError('minlength')) {
      return 'Le champ doit avoir au moins 3 caractères';
    }
    if (this.nom.hasError('required')) {
      return 'Le champ doit être rempli et avoir au moins 3 caractères';
    }
    return '';
  }

  save(): void {
    this.usersService.createUser(this.userToSend).subscribe(res => {
        console.log(res);
      },
      error => {
        console.log(error);
      }, () => {
      });
    this.alertNotif()
  }

  alertNotif(): void {
    this.toastr.success('L\'utilisateur a bien été enregistré.', 'Enregistrement', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }

}

export class User {
  constructor(
    public idUser: number | null,
    public nom: string,
    public prenom: string,
    public mail: string,
    // tslint:disable-next-line:variable-name
    public FK_equipe: number | undefined,
    // tslint:disable-next-line:variable-name
    public FK_role: number | undefined,
    public BE: number | undefined,
    public tracabilite: any,
  ) {
  }
}
