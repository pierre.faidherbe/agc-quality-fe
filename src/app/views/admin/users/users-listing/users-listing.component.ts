import {Component, OnInit} from '@angular/core';
import {UsersService, User, Equipe, Role} from '../../../../services/users/users.service';
import {ToastrService} from "ngx-toastr";

interface UserToSend {
  idUser: number | null;
  nom: string;
  prenom: string;
  equipe: string;
  role: string;
  mail: string;
  obsolete: boolean;
  BE: number | undefined;
  tracabilite: any;
}

@Component({
  selector: 'app-users-listing',
  templateUrl: './users-listing.component.html',
  styleUrls: ['./users-listing.component.css']
})
export class UsersListingComponent implements OnInit {
  UsersList: User[] = [];
  UsersData: UserToSend[] = [];
  EquipesList: Equipe[] = [];
  RolesList: Role[] = [];

  temp: UserToSend[] = [];

  constructor(
    private usersService: UsersService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.getEquipe();
  }

  getEquipe(): void {
    this.usersService.getEquipes().subscribe(res => {
        this.EquipesList = res;
      }, error => {
      },
      () => {
        this.getRole();
      });
  }

  getRole(): void {
    this.usersService.getRoles().subscribe(res => {
        this.RolesList = res;
      }, error => {
      },
      () => {
        this.getData();
      });
  }

  getData(): void {
    this.usersService.getUsers().subscribe(res => {
        this.UsersList = res;
        for (const entry of this.UsersList) {
          // @ts-ignore
          let roleStr;
          let EquipeStr;
          for (let role of this.RolesList) {
            if (entry.FK_role === role.roleId) {
              roleStr = role.role;
            }
          }
          for (let equipe of this.EquipesList) {
            if (entry.FK_equipe === equipe.equipeId) {
              EquipeStr = equipe.equipe;
            }
          }
          this.UsersData.push({
            idUser: entry.idUser,
            nom: entry.nom,
            prenom: entry.prenom,
            mail: entry.mail === null ? 'undefined' : entry.mail,
            BE: entry.BE,
            // @ts-ignore
            obsolete: entry.obsolete,
            // @ts-ignore
            role: roleStr,
            // @ts-ignore
            equipe: EquipeStr,
            tracabilite: entry.tracabilite === null ? 'undefined' : entry.tracabilite
          });
        }
      }, error => {
      },
      () => {
        this.UsersData = [...this.UsersData];
        this.temp = this.UsersData;
      });
  }

  filterData(event: any): void {
    let val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      return d.nom.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.UsersData = temp;
  }

  changeStatut(obsolete: boolean, id: number): void {
    let jsonToSend = {obsolete, id};
    this.UsersList.length = 0;
    this.UsersData.length = 0;
    this.usersService.deleteUser(jsonToSend).subscribe(res => {
        this.getData();
      }, error => {
      },
      () => {
        this.alertNotif(obsolete)
      });
  }

  alertNotif(obsolete: boolean): void {
    if (obsolete) {
      this.toastr.success('L\'utilisateur a bien été désactivé.', 'Désactivation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    } else {
      this.toastr.success('L\'utilisateur a bien été activé.', 'Activation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    }
  }

}
