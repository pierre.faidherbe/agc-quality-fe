import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipesListingComponent } from './equipes-listing.component';

describe('EquipesListingComponent', () => {
  let component: EquipesListingComponent;
  let fixture: ComponentFixture<EquipesListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquipesListingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipesListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
