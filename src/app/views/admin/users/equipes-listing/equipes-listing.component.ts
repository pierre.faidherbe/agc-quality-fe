import {Component, OnInit} from '@angular/core';
import {Equipe, UsersService} from '../../../../services/users/users.service';
import {MAT_TOOLTIP_DEFAULT_OPTIONS, MatTooltipDefaultOptions} from '@angular/material/tooltip';
import {ToastrService} from "ngx-toastr";

export const myCustomTooltipDefaults: MatTooltipDefaultOptions = {
  showDelay: 1000,
  hideDelay: 1000,
  touchendHideDelay: 1000,
};

@Component({
  selector: 'app-equipes-listing',
  templateUrl: './equipes-listing.component.html',
  styleUrls: ['./equipes-listing.component.css'],
  providers: [
    {provide: MAT_TOOLTIP_DEFAULT_OPTIONS, useValue: myCustomTooltipDefaults}
  ],
})
export class EquipesListingComponent implements OnInit {
  EquipesData: EquipeView[] = [];
  temp: EquipeView[] = [];

  constructor(
    private UserService: UsersService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.UserService.getEquipes().subscribe(res => {
        for (let entry of res) {
          this.EquipesData.push({
            equipe: entry.equipe,
            equipeId: entry.equipeId,
            // @ts-ignore
            obsolete: entry.obsolete
          });
        }
      }, error => {
      },
      () => {
        this.EquipesData = [...this.EquipesData];
        this.temp = this.EquipesData;
      });
  }

  filterData(event: any): void {
    const val = event.target.value.toLowerCase();
    // tslint:disable-next-line:only-arrow-functions
    const temp = this.temp.filter(function (d) {
      return d.equipe.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.EquipesData = temp;
  }

  changeStatut(obsolete: boolean, id: number): void {
    let jsonToSend = {obsolete, id};
    this.EquipesData.length = 0;
    this.UserService.deleteEquipe(jsonToSend).subscribe(res => {
        this.getData();
      }, error => {
      },
      () => {
        this.alertNotif(obsolete)
      });
  }

  alertNotif(obsolete: boolean): void {
    if (obsolete) {
      this.toastr.success('L\'equipe a bien été désactivée.', 'Désactivation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    } else {
      this.toastr.success('L\'equipe a bien été activée.', 'Activation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    }
  }
}

export interface EquipeView {
  equipeId: number | null;
  equipe: string;
  obsolete: boolean;
}
