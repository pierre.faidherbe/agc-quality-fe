import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Equipe, Role, UsersService} from '../../../../services/users/users.service';
import {ActivatedRoute} from '@angular/router';
import {User} from '../post-users/post-users.component';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-users-update',
  templateUrl: './users-update.component.html',
  styleUrls: ['./users-update.component.css']
})
export class UsersUpdateComponent implements OnInit {
  email = new FormControl('', [Validators.required, Validators.email]);
  nom = new FormControl('', [Validators.required, Validators.minLength(3)]);
  prenom = new FormControl('', [Validators.required, Validators.minLength(3)]);
  selectEquipe = new FormControl('', [Validators.required]);
  selectRole = new FormControl('', [Validators.required]);
  equipesList: Equipe[] = [];
  roleList: Role[] = [];

  // @ts-ignore
  userToUpdate: User;
  id = 0;

  constructor(
    private usersService: UsersService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.id = parseInt(this.route.snapshot.paramMap.get('id') as string, 10);
    this.userToUpdate = new User(null, '', '', '', undefined, undefined, undefined, null);
    this.getEquipesList();
    this.getRoleList();
    this.getUserToUpdate();
  }

  getEquipesList(): void {
    this.usersService.getEquipes().subscribe(res => {
      this.equipesList = res;
    });
  }

  getRoleList(): void {
    this.usersService.getRoles().subscribe(res => {
      this.roleList = res;
    });
  }

  getErrorMessage(): string {
    if (this.email.hasError('required')) {
      return 'Le champs doit être rempli';
    }
    if (this.email.hasError('email')) {
      return 'l\'email n\'est pas valide';
    }
    return '';
  }

  getSelectEquipeErrorMessage(): string {
    if (this.selectEquipe.hasError('required')) {
      return 'Selectionnez une équipe';
    }
    return '';
  }

  getSelectRoleErrorMessage(): string {
    if (this.selectRole.hasError('required')) {
      return 'Selectionnez un role';
    }
    return '';
  }

  getNomErrorMsg(): string {
    if (this.nom.hasError('minlength')) {
      return 'Le champ doit avoir au moins 3 caractères';
    }
    if (this.nom.hasError('required')) {
      return 'Le champ doit être rempli et avoir au moins 3 caractères';
    }
    return '';
  }

  getUserToUpdate(): void {
    this.usersService.getUserById(this.id).subscribe(res => {
      this.userToUpdate = res;
    });
  }

  save(): void {
    this.usersService.updateUser(this.userToUpdate).subscribe(res => {
      console.log(res);
    }, error => {
    }, () => {
    });
    this.alertNotif();
  }

  alertNotif(): void {
    this.toastr.success('L\'utilisateur a bien été modifié.', 'Update', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }
}
