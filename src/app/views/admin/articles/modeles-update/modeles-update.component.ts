import {Component, OnInit} from '@angular/core';
import {ArticlesService, Modele} from '../../../../services/articles/articles.service';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-modeles-update',
  templateUrl: './modeles-update.component.html',
  styleUrls: ['./modeles-update.component.css']
})
export class ModelesUpdateComponent implements OnInit {
  // @ts-ignore
  modeleToUpdate: Modele;
  // @ts-ignore
  idModele: any;

  constructor(
    private articleService: ArticlesService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.idModele = this.route.snapshot.paramMap.get('id');
    this.getModeleToUpdate();
  }

  getModeleToUpdate(): void {
    console.log(this.idModele);
    this.articleService.getModele(this.idModele).subscribe(res => {
      this.modeleToUpdate = res;
    });
  }

  save(): void {
    this.articleService.updateModele(this.modeleToUpdate).subscribe(res => {
      console.log(res);
    }, error => {
      console.log(error);
    }, () => {
    });
    this.alertNotif();
  }

  alertNotif(): void {
    this.toastr.success('Le modèle a bien été modifié.', 'Update', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }
}
