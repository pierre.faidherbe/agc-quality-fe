import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelesUpdateComponent } from './modeles-update.component';

describe('ModelesUpdateComponent', () => {
  let component: ModelesUpdateComponent;
  let fixture: ComponentFixture<ModelesUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModelesUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelesUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
