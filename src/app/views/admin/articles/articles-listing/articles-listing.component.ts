import {Component, OnInit} from '@angular/core';
import {Article, ArticlesService, Modele} from '../../../../services/articles/articles.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-articles-listing',
  templateUrl: './articles-listing.component.html',
  styleUrls: ['./articles-listing.component.css']
})
export class ArticlesListingComponent implements OnInit {
  modelesListing: Modele[] = [];
  articlesListing: Article[] = [];
  articlesViews: ArticleView[] = [];
  temp: ArticleView[] = [];

  constructor(
    private articleService: ArticlesService,
    private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.getModele();
    this.getData();
  }

  getData(): void {
    this.articleService.getArticles().subscribe(res => {
      this.articlesListing = res;
      for (let entry of this.articlesListing) {
        this.articlesViews.push({
          articleId: entry.articleId,
          codeArticle: entry.codeArticle,
          // @ts-ignore
          articleStatus: entry.obsolete,
          fk_model: entry.fk_model,
          model: this.modelesListing[
            // @ts-ignore
          entry.fk_model - 1
            ].modele,
          descriptionSAP: entry.descriptionSAP,
          partNumber: entry.partNumber,
        });
      }
      this.articlesViews = [...this.articlesViews];
      this.temp = this.articlesViews;
      console.log(this.articlesViews);
    });
  }

  getModele(): void {
    this.articleService.getModeles().subscribe(res => {
      this.modelesListing = res;
    });
  }

  filterData(event: any): void {
    let val = event.target.value.toLowerCase();
    // tslint:disable-next-line:only-arrow-functions
    const temp = this.temp.filter(function (d) {
      // @ts-ignore
      return d.codeArticle.toString().indexOf(val) !== -1 || !val;
    });
    this.articlesViews = temp;
  }

  changeStatut(obsolete: boolean, id: number): void {
    let jsonToSend = {obsolete, id};
    this.articleService.deleteArticle(jsonToSend).subscribe(res => {
        this.articlesViews.length = 0;
        this.getData();
      }, error => {
      },
      () => {
        this.alertNotif(obsolete);
      });
  }

  alertNotif(obselete: boolean): void {
    if(obselete){
      this.toastr.success('L\'article a bien été désactivé.', 'Désactivation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    }else {
      this.toastr.success('L\'article a bien été activé.', 'Activation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    }
  }

}

export interface ArticleView {
  articleId: number | null;
  codeArticle: number | null;
  fk_model: number | null;
  model: string | null;
  descriptionSAP: string;
  partNumber: string;
}
