import {Component, OnInit} from '@angular/core';
import {ArticlesService, Modele} from '../../../../services/articles/articles.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-articles-post',
  templateUrl: './articles-post.component.html',
  styleUrls: ['./articles-post.component.css']
})
export class ArticlesPostComponent implements OnInit {
  // @ts-ignore
  newArticle: ArticleToSend;
  modeleList: Modele[] = [];

  constructor(
    private articlesService: ArticlesService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.newArticle = new ArticleToSend(null, null, null, '', '');
    this.getModeleList();
  }

  getModeleList(): void {
    this.articlesService.getModeles().subscribe(res => {
      this.modeleList = res;
    });
  }

  save(): void {
    this.articlesService.createArticle(this.newArticle).subscribe(res => {
        console.log(res);
      },
      error => {
        console.log(error);
      }, () => {
      });
    this.alertNotif();
  }

  alertNotif(): void {
    this.toastr.success('L\'article a bien été enregistré.', 'Enregistrement', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }
}


export class ArticleToSend {
  constructor(
    public articleId: number | null,
    public codeArticle: number | null,
    // tslint:disable-next-line:variable-name
    public fk_model: number | null,
    public descriptionSAP: string,
    public partNumber: string
  ) {
  }
}
