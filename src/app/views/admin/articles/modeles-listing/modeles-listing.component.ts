import {Component, OnInit} from '@angular/core';
import {ArticlesService, Modele} from '../../../../services/articles/articles.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-modeles-listing',
  templateUrl: './modeles-listing.component.html',
  styleUrls: ['./modeles-listing.component.css']
})
export class ModelesListingComponent implements OnInit {
  ModelesData: Modele[] = [];
  modelesView: ModeleView[] = [];
  temp: ModeleView[] = [];

  constructor(
    private articlesService: ArticlesService,
    private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.getModeles();
  }

  getModeles(): void {
    this.articlesService.getModeles().subscribe(res => {
        this.ModelesData = res;
        this.ModelesData = [...this.ModelesData];
      }, error => {
      },
      () => {
        for (let entry of this.ModelesData) {
          this.modelesView.push({
            modele: entry.modele,
            modeleId: entry.modeleId,
            // @ts-ignore
            modeleObsolete: entry.obsolete
          });
        }
        this.modelesView = [...this.modelesView];
        this.temp = this.modelesView;
      });
  }

  filterData(event: any): void {
    const val = event.target.value.toLowerCase();
    // tslint:disable-next-line:only-arrow-functions
    const temp = this.temp.filter(function (d) {
      return d.modele.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.modelesView = temp;
  }

  changeStatut(obsolete: boolean, id: number): void {
    let jsonToSend = {obsolete, id};
    this.articlesService.deleteModele(jsonToSend).subscribe(res => {
        this.modelesView.length = 0;
        this.getModeles();
      }, error => {
      },
      () => {
      this.alertNotif(obsolete);
      });
  }

  alertNotif(obsolete: boolean): void {
    if (obsolete) {
      this.toastr.success('Le modèle a bien été désactivé.', 'Désactivation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    } else {
      this.toastr.success('Le modèle a bien été activé.', 'Activation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    }
  }
}

export interface ModeleView {
  modeleId: number | null;
  modeleObsolete: boolean;
  modele: string;
}
