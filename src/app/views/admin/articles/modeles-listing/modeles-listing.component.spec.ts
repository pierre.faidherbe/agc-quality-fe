import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelesListingComponent } from './modeles-listing.component';

describe('ModelesListingComponent', () => {
  let component: ModelesListingComponent;
  let fixture: ComponentFixture<ModelesListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModelesListingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelesListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
