import {Component, OnInit} from '@angular/core';
import {Article, ArticlesService, Modele} from '../../../../services/articles/articles.service';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-articles-update',
  templateUrl: './articles-update.component.html',
  styleUrls: ['./articles-update.component.css']
})
export class ArticlesUpdateComponent implements OnInit {
  // @ts-ignore
  articleToUpdate: Article;
  // @ts-ignore
  temp: ArticleToSend;
  idArticle: any;
  modeleList: Modele[] = [];

  constructor(
    private articleService: ArticlesService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.idArticle = this.route.snapshot.paramMap.get('id');
    this.getModeleList();
    this.temp = new ArticleToSend(null, null, null, '', '');
  }

  getModeleList(): void {
    this.articleService.getModeles().subscribe(res => {
      this.modeleList = res;
    }, error => {
      console.log(error);
    }, () => {
      this.getArticleToUpdate();
    });
  }

  getArticleToUpdate(): void {
    this.articleService.getArticle(this.idArticle).subscribe(res => {
      this.articleToUpdate = res;
    });
  }

  save(): void {
    this.temp.fk_model = this.articleToUpdate.fk_model;
    this.temp.codeArticle = this.articleToUpdate.codeArticle;
    this.temp.articleId = this.articleToUpdate.articleId;
    this.temp.descriptionSAP = this.articleToUpdate.descriptionSAP;
    this.temp.partNumber = this.articleToUpdate.partNumber;
    this.articleService.updateArticle(this.temp).subscribe(res => {
      console.log(res);
    }, error => {
      console.log(error);
    }, () => {
    });
    this.alertNotif();
  }

  alertNotif(): void {
    this.toastr.success('L\'article a bien été modifié.', 'Update', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }
}

export class ArticleToSend {
  constructor(
    public articleId: number | null,
    public codeArticle: number | null,
    // tslint:disable-next-line:variable-name
    public fk_model: number | null,
    public descriptionSAP: string,
    public partNumber: string
  ) {
  }
}

