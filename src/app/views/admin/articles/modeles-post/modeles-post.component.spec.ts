import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelesPostComponent } from './modeles-post.component';

describe('ModelesPostComponent', () => {
  let component: ModelesPostComponent;
  let fixture: ComponentFixture<ModelesPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModelesPostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelesPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
