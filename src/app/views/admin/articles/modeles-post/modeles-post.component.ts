import {Component, OnInit} from '@angular/core';
import {ArticlesService, Modele} from '../../../../services/articles/articles.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-modeles-post',
  templateUrl: './modeles-post.component.html',
  styleUrls: ['./modeles-post.component.css']
})
export class ModelesPostComponent implements OnInit {
  // @ts-ignore
  newModele: ModeleToSend;

  constructor(
    private articlesService: ArticlesService,
    private toastr: ToastrService
    ) {
  }

  ngOnInit(): void {
    this.newModele = new ModeleToSend(null, '');
  }

  save(): void {
    this.articlesService.createModele(this.newModele).subscribe(res => {
        console.log(res);
      }, error => {
        console.log(error);
      },
      () => {
      });
    this.alertNotif();
  }


  alertNotif(): void {
    this.toastr.success('Le modèle a bien été enregistré.', 'Enregistrement', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }
}

export class ModeleToSend {
  constructor(
    public modeleId: number | null,
    public modele: string
  ) {
  }
}

