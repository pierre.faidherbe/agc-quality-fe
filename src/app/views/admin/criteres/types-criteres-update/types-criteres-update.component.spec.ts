import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypesCriteresUpdateComponent } from './types-criteres-update.component';

describe('TypesCriteresUpdateComponent', () => {
  let component: TypesCriteresUpdateComponent;
  let fixture: ComponentFixture<TypesCriteresUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypesCriteresUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypesCriteresUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
