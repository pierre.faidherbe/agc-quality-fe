import {Component, OnInit} from '@angular/core';
import {CriteresService} from '../../../../services/criteres/criteres.service';
import {ActivatedRoute} from '@angular/router';
import {TypeCritere} from '../types-criteres-post/types-criteres-post.component';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-types-criteres-update',
  templateUrl: './types-criteres-update.component.html',
  styleUrls: ['./types-criteres-update.component.css']
})
export class TypesCriteresUpdateComponent implements OnInit {
  // @ts-ignore
  typeCritToUpdate: TypeCritere;
  id = 0;

  constructor(
    private criteresService: CriteresService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.typeCritToUpdate = new TypeCritere(null, '');
    this.id = parseInt(this.route.snapshot.paramMap.get('id') as string, 10);
    this.getTypeCritToUpdate();
  }

  getTypeCritToUpdate(): void {
    this.criteresService.getTypeCritereById(this.id).subscribe(res => {
      this.typeCritToUpdate = res;
    });
  }

  save(): void {
    this.criteresService.updateTypeCritere(this.typeCritToUpdate).subscribe(res => {
        console.log(res);
      }, error => {
      },
      () => {
      });
    this.alertNotif();
  }

  alertNotif(): void {
    this.toastr.success('Le type de critère a bien été modifié.', 'Update', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }
}
