import {Component, OnInit} from '@angular/core';
import {CriteresService, TypeCritere} from '../../../../services/criteres/criteres.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-types-criteres-listing',
  templateUrl: './types-criteres-listing.component.html',
  styleUrls: ['./types-criteres-listing.component.css']
})
export class TypesCriteresListingComponent implements OnInit {
  typesCriteresListing: TypeCritereView[] = [];
  temp: TypeCritereView[] = [];

  constructor(
    private criteresServices: CriteresService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.criteresServices.getTypeCritere().subscribe(res => {
        for (let entry of res) {
          this.typesCriteresListing.push({
            type: entry.type,
            typeCritereId: entry.typeCritereId,
            // @ts-ignore
            obsolete: entry.obsolete
          });
        }
      }, error => {
      },
      () => {
        this.typesCriteresListing = [...this.typesCriteresListing];
        this.temp = this.typesCriteresListing;
      });
  }

  filterData(event: any): void {
    const val = event.target.value.toLowerCase();
    // tslint:disable-next-line:only-arrow-functions
    const temp = this.temp.filter(function (d) {
      return d.type.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.typesCriteresListing = temp;
  }

  changeStatut(obsolete: boolean, id: number): void {
    let jsonToSend = {obsolete, id};
    this.typesCriteresListing.length = 0;
    this.criteresServices.deleteTypeCritere(jsonToSend).subscribe(res => {
        this.getData();
      }, error => {
      },
      () => {
        this.alertNotif(obsolete);
      });
  }

  alertNotif(obsolete: boolean): void {
    if (obsolete) {
      this.toastr.success('Le type de critère a bien été désactivé.', 'Désactivation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    } else {
      this.toastr.success('Le type de critère a bien été activé.', 'Activation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    }
  }

}

export interface TypeCritereView {
  typeCritereId: number | null;
  type: string;
  obsolete: boolean;
}
