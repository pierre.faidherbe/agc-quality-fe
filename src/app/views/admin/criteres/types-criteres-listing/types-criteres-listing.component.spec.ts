import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypesCriteresListingComponent } from './types-criteres-listing.component';

describe('TypesCriteresListingComponent', () => {
  let component: TypesCriteresListingComponent;
  let fixture: ComponentFixture<TypesCriteresListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypesCriteresListingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypesCriteresListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
