import {Component, OnInit} from '@angular/core';
import {
  Criteres,
  CriteresService,
  Process,
  ProcessCriteresRel,
  TypeCritere
} from '../../../../services/criteres/criteres.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-criteres-listing',
  templateUrl: './criteres-listing.component.html',
  styleUrls: ['./criteres-listing.component.css']
})
export class CriteresListingComponent implements OnInit {
  typeCriteresList: TypeCritere[] = [];
  processList: Process[] = [];
  processCritRelList: ProcessCriteresRel[] = [];
  criteresListing: Criteres[] = [];
  criteresData: CriteresView[] = [];
  temp: CriteresView[] = [];

  constructor(
    private criteresService: CriteresService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.getProcessCritRel();
  }

  getTypeCriteres(): void {
    this.criteresService.getTypeCritere().subscribe(res => {
      this.typeCriteresList = res;
    }, error => {
      console.log(error);
    }, () => {
      this.getData();
    });
  }

  getAllProcess(): void {
    this.criteresService.getProcess().subscribe(res => {
      this.processList = res;
    }, error => {
      console.log(error);
    }, () => {
      this.getTypeCriteres();
    });
  }

  getProcessCritRel(): void {
    this.criteresService.getProcessCriteresRelation().subscribe(res => {
      this.processCritRelList = res;
    }, error => {
      console.log(error);
    }, () => {
      this.getAllProcess();
    });
  }

  getData(): void {
    this.criteresService.getCriteres().subscribe(res => {
      this.criteresListing = res;
    }, error => {
      console.log(error);
    }, () => {
      for (let entry of this.criteresListing) {
        let CriteresProcess = [];
        for (let processCrit of this.processCritRelList) {
          if (processCrit.FK_critereId === entry.critereId) {
            for (let process of this.processList) {
              if (processCrit.FK_processId === process.processId) {
                CriteresProcess.push(process.nomProcess);
              }
            }
          }
        }
        this.criteresData.push({
          critereId: entry.critereId,
          nomCritere: entry.nomCritere,
          typeObservation: entry.typeObservation,
          infoDemerite: entry.infoDemerite,
          // @ts-ignore
          obsolete: entry.obsolete,
          FK_typeCriteres: entry.FK_TypeCriteres,
          typeCriteres: this.typeCriteresList[entry.FK_TypeCriteres - 1].type,
          processList: CriteresProcess
        });
      }
      this.criteresData = [...this.criteresData];
      this.temp = this.criteresData;
    });
  }

  filterData(event: any): void {
    const val = event.target.value.toLowerCase();
    // tslint:disable-next-line:only-arrow-functions
    const temp = this.temp.filter(function (d) {
      return d.nomCritere.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.criteresData = temp;
  }

  changeStatut(obsolete: boolean, id: number): void {
    let jsonToSend = {obsolete, id};
    this.criteresListing.length = 0;
    this.criteresData.length = 0;
    this.criteresService.deleteCritere(jsonToSend).subscribe(res => {
        this.getData();
      }, error => {
      },
      () => {
        this.alertNotif(obsolete);
      });
  }


  alertNotif(obsolete: boolean): void {
    if (obsolete) {
      this.toastr.success('Le critère a bien été désactivé.', 'Désactivation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    } else {
      this.toastr.success('Le critère a bien été activé.', 'Activation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    }
  }

}

export interface CriteresView {
  critereId: number;
  nomCritere: string;
  typeObservation: string;
  obsolete: boolean;
  infoDemerite: string;
  FK_typeCriteres: number;
  typeCriteres: string;
  processList: any[] | undefined;
}
