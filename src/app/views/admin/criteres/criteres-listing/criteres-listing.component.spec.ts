import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriteresListingComponent } from './criteres-listing.component';

describe('CriteresListingComponent', () => {
  let component: CriteresListingComponent;
  let fixture: ComponentFixture<CriteresListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriteresListingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriteresListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
