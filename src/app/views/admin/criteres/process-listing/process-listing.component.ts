import {Component, OnInit} from '@angular/core';
import {CriteresService, Process} from '../../../../services/criteres/criteres.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-process-listing',
  templateUrl: './process-listing.component.html',
  styleUrls: ['./process-listing.component.css']
})
export class ProcessListingComponent implements OnInit {
  processListing: ProcessView[] = [];
  temp: ProcessView[] = [];

  constructor(
    private criteresServices: CriteresService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.criteresServices.getProcess().subscribe(res => {
        for (let entry of res) {
          this.processListing.push({
            processId: entry.processId,
            nomProcess: entry.nomProcess,
            // @ts-ignore
            obsolete: entry.obsolete
          });
        }
      }, error => {
      },
      () => {
        this.processListing = [...this.processListing];
        this.temp = this.processListing;
      });
  }

  filterData(event: any): void {
    const val = event.target.value.toLowerCase();
    // tslint:disable-next-line:only-arrow-functions
    const temp = this.temp.filter(function (d) {
      return d.nomProcess.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.processListing = temp;
  }

  changeStatut(obsolete: boolean, id: number): void {
    let jsonToSend = {obsolete, id};
    this.processListing.length = 0;
    this.criteresServices.deleteProcess(jsonToSend).subscribe(res => {
        this.getData();
      }, error => {
      },
      () => {
      });
    this.alertNotif(obsolete);
  }

  alertNotif(obsolete: boolean): void {
    if (obsolete) {
      this.toastr.success('Le process a bien été désactivé.', 'Désactivation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    } else {
      this.toastr.success('Le process a bien été activé.', 'Activation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    }
  }
}

export interface ProcessView {
  processId: number | null;
  nomProcess: string;
  obsolete: boolean;
}
