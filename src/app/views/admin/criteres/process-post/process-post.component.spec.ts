import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessPostComponent } from './process-post.component';

describe('ProcessPostComponent', () => {
  let component: ProcessPostComponent;
  let fixture: ComponentFixture<ProcessPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProcessPostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
