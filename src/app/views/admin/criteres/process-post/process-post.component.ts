import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {CriteresService} from '../../../../services/criteres/criteres.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-process-post',
  templateUrl: './process-post.component.html',
  styleUrls: ['./process-post.component.css']
})
export class ProcessPostComponent implements OnInit {
  // @ts-ignore
  newProcess: Process;
  process = new FormControl('', [Validators.required, Validators.minLength(3)]);

  constructor(
    private criteresService: CriteresService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.newProcess = new Process(null, '');
  }

  getErrorMessage(): string {
    if (this.process.hasError('required')) {
      return 'Le champs doit être rempli et avoir au moins 3 caractères';
    }
    if (this.process.hasError('minlenght')) {
      return 'Le champs doit avoir au moins 3 caractères';
    }
    return '';
  }

  save(): void {
    console.log(this.newProcess);
    this.criteresService.createProcess(this.newProcess).subscribe(res => {
      console.log(res);
      this.alertNotif();
    }, error => {
      console.log(error);
      this.alertNotif();

    }, () => {
      this.alertNotif();
    });
  }

  alertNotif(): void {
    this.toastr.success('Le process a bien été enregistré.', 'Enregistrement', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }

}

export class Process {
  constructor(
    public processId: number | null,
    public nomProcess: string
  ) {
  }
}
