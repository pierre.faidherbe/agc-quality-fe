import {Component, OnInit} from '@angular/core';
import {CriteresService, Process, TypeCritere} from '../../../../services/criteres/criteres.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-criteres-post',
  templateUrl: './criteres-post.component.html',
  styleUrls: ['./criteres-post.component.css']
})
export class CriteresPostComponent implements OnInit {
  processList: Process[] = [];
  typeCriteresList: TypeCritere[] = [];
  // @ts-ignore
  criteresToSend: CritereToSend;

  constructor(
    private criteresService: CriteresService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    // @ts-ignore
    this.criteresToSend = new CritereToSend(null, '', '', '', null, []);
    this.getTypeCriteresList();
    this.getProcessList();
  }

  getTypeCriteresList(): void {
    this.criteresService.getTypeCritere().subscribe(res => {
      this.typeCriteresList = res;
    });
  }

  getProcessList(): void {
    this.criteresService.getProcess().subscribe(res => {
      this.processList = res;
    });
  }

  save(): void {
    this.criteresService.createCritere(this.criteresToSend).subscribe(res => {
        console.log(res);
      },
      error => {
        console.log(error);
      },
      () => {
      });
    this.alertNotif();
  }

  alertNotif(): void {
    this.toastr.success('Le critère a bien été enregistré.', 'Enregistrement', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }
}

export class CritereToSend {
  constructor(
    public critereId: number | null,
    public  nomCritere: string,
    public typeObservation: string,
    public infoDemerite: string,
    // tslint:disable-next-line:variable-name
    public FK_TypeCriteres: number | null,
    public listProcess: number[],
  ) {
  }
}
