import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriteresPostComponent } from './criteres-post.component';

describe('CriteresPostComponent', () => {
  let component: CriteresPostComponent;
  let fixture: ComponentFixture<CriteresPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriteresPostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriteresPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
