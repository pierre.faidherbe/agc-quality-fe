import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriteresUpdateComponent } from './criteres-update.component';

describe('CriteresUpdateComponent', () => {
  let component: CriteresUpdateComponent;
  let fixture: ComponentFixture<CriteresUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriteresUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriteresUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
