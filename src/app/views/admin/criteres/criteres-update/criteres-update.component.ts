import {Component, OnInit} from '@angular/core';
import {
  CriteresService,
  Process,
  ProcessCriteresRel,
  TypeCritere
} from '../../../../services/criteres/criteres.service';
import {ActivatedRoute} from '@angular/router';
import {CritereToSend} from '../criteres-post/criteres-post.component';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-criteres-update',
  templateUrl: './criteres-update.component.html',
  styleUrls: ['./criteres-update.component.css']
})
export class CriteresUpdateComponent implements OnInit {
  // @ts-ignore
  critereToUpdate: CritereToSend;
  // @ts-ignore
  temp: Criteres;
  processList: Process[] = [];
  typeCriteresList: TypeCritere[] = [];
  relationsCritProcess: ProcessCriteresRel[] = [];
  id = 0;

  constructor(
    private criteresService: CriteresService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.critereToUpdate = new CritereToSend(null, '', '', '', null, []);
    this.id = parseInt(this.route.snapshot.paramMap.get('id') as string, 10);
    this.getProcessList();
  }

  getProcessList(): void {
    this.criteresService.getProcess().subscribe(res => {
        this.processList = res;
      }, error => {
      },
      () => {
        this.getTypeCriteresList();
      });
  }

  getTypeCriteresList(): void {
    this.criteresService.getTypeCritere().subscribe(res => {
        this.typeCriteresList = res;
      }, error => {
      },
      () => {
        this.getRelationsCritProcess();
      });
  }

  getRelationsCritProcess(): void {
    this.criteresService.getProcessCriteresRelation().subscribe(res => {
        this.relationsCritProcess = res;
      }, error => {
      },
      () => {
        this.getData();
      });
  }

  getData(): void {
    this.criteresService.getCritereById(this.id).subscribe(res => {
        this.temp = res;
      }, error => {
      },
      () => {
        let CritprocessList: number[] = [];
        for (let rel of this.relationsCritProcess) {
          if (this.temp.critereId === rel.FK_critereId) {
            CritprocessList.push(rel.FK_processId);
          }
        }
        this.critereToUpdate.critereId = this.temp.critereId;
        this.critereToUpdate.FK_TypeCriteres = this.temp.FK_TypeCriteres;
        this.critereToUpdate.infoDemerite = this.temp.infoDemerite;
        this.critereToUpdate.nomCritere = this.temp.nomCritere;
        this.critereToUpdate.typeObservation = this.temp.typeObservation;
        this.critereToUpdate.listProcess = CritprocessList;
      });
  }

  save(): void {
    this.criteresService.updateCritere(this.critereToUpdate).subscribe(res => {
        console.log(res);

      }, error => {
        console.log(error)
      },
      () => {
      });
    this.alertNotif();
  }

  alertNotif(): void {
    this.toastr.success('Le critère a bien été modifié.', 'Update', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }
}
