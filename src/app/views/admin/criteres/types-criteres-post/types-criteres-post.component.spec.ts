import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypesCriteresPostComponent } from './types-criteres-post.component';

describe('TypesCriteresPostComponent', () => {
  let component: TypesCriteresPostComponent;
  let fixture: ComponentFixture<TypesCriteresPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypesCriteresPostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypesCriteresPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
