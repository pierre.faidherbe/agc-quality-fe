import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {CriteresService} from '../../../../services/criteres/criteres.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-types-criteres-post',
  templateUrl: './types-criteres-post.component.html',
  styleUrls: ['./types-criteres-post.component.css']
})
export class TypesCriteresPostComponent implements OnInit {
  // @ts-ignore
  newTypeCrit: TypeCritere;
  typeCrit = new FormControl('', [Validators.required, Validators.minLength(3)]);

  constructor(
    private criteresService: CriteresService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.newTypeCrit = new TypeCritere(null, '');
  }

  getErrorMessage(): string {
    if (this.typeCrit.hasError('required')) {
      return 'Le champs doit être rempli et avoir au moins 3 caractères';
    }
    if (this.typeCrit.hasError('minlenght')) {
      return 'Le champs doit avoir au moins 3 caractères';
    }
    return '';
  }

  save(): void {
    this.criteresService.createTypeCritere(this.newTypeCrit).subscribe(res => {
      console.log(res);
    }, error => {
      console.log(error);
    }, () => {
    });
    this.alertNotif();
  }

  alertNotif(): void {
    this.toastr.success('Le type de critère a bien été enregistré.', 'Enregistrement', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }
}


export class TypeCritere {
  constructor(
    public typeCritereId: number | null,
    public type: string
  ) {
  }
}
