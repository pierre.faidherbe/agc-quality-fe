import {Component, OnInit} from '@angular/core';
import {CriteresService, Process} from '../../../../services/criteres/criteres.service';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-process-update',
  templateUrl: './process-update.component.html',
  styleUrls: ['./process-update.component.css']
})
export class ProcessUpdateComponent implements OnInit {
  // @ts-ignore
  processToUpdate: Process;
  id = 0;

  constructor(
    private criteresService: CriteresService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.id = parseInt(this.route.snapshot.paramMap.get('id') as string, 10);
    this.getProcessToUpdate();
  }

  getProcessToUpdate(): void {
    this.criteresService.getProcessById(this.id).subscribe(res => {
      this.processToUpdate = res;
    });
  }

  save(): void {
    this.criteresService.updateProcess(this.processToUpdate).subscribe(res => {
        console.log(res);
      }, error => {
      },
      () => {
        this.alertNotif();
      });
  }

  alertNotif(): void {
    this.toastr.success('Le process a bien été modifié.', 'Update', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }
}
