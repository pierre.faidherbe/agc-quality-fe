import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectifAnnuelPostComponent } from './objectif-annuel-post.component';

describe('ObjectifAnnuelPostComponent', () => {
  let component: ObjectifAnnuelPostComponent;
  let fixture: ComponentFixture<ObjectifAnnuelPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ObjectifAnnuelPostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectifAnnuelPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
