import {Component, OnInit} from '@angular/core';
import {AuditService} from '../../../../services/audit/audit.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-objectif-annuel-post',
  templateUrl: './objectif-annuel-post.component.html',
  styleUrls: ['./objectif-annuel-post.component.css']
})
export class ObjectifAnnuelPostComponent implements OnInit {
  // @ts-ignore
  objectif: Objectif;

  constructor(
    private auditService: AuditService,
    private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.objectif = new Objectif(null, null, false);
  }

  save(): void {
    this.auditService.createObjectif(this.objectif).subscribe(res => {
      console.log(res);
    }, error => {
      console.log(error);
    }, () => {
    });
    this.alertNotif();
  }

  alertNotif(): void {
    this.toastr.success('L\'objectif a bien été enregistré.', 'Enregistrement', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }

}

export class Objectif {
  constructor(
    public objectifId: number | null,
    public objectif: number | null,
    public actif: boolean,
  ) {
  }
}
