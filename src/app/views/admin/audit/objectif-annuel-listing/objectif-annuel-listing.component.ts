import {Component, OnInit} from '@angular/core';
import {AuditService, Objectif} from '../../../../services/audit/audit.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-objectif-annuel-listing',
  templateUrl: './objectif-annuel-listing.component.html',
  styleUrls: ['./objectif-annuel-listing.component.css']
})
export class ObjectifAnnuelListingComponent implements OnInit {
  objectifsListing: Objectif[] = [];
  temp: Objectif[] = [];

  constructor(
    private auditService: AuditService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.auditService.getObjectifs().subscribe(res => {
      this.objectifsListing = res;
      this.objectifsListing = [...this.objectifsListing];
      this.temp = this.objectifsListing;
    });
  }

  filterData(event: any): void {
    let val = event.target.value.toLowerCase();
    // tslint:disable-next-line:only-arrow-functions
    const temp = this.temp.filter(function (d) {
      // @ts-ignore
      return d.objectif.toString().indexOf(val) !== -1 || !val;
    });
    this.objectifsListing = temp;
  }

  changeStatut(obsolete: boolean, id: number): void {
    let jsonToSend = {obsolete, id};
    this.objectifsListing.length = 0;
    this.auditService.deleteObjectif(jsonToSend).subscribe(res => {
        this.getData();
      }, error => {
      },
      () => {
      this.alertNotif(obsolete)
      });
  }

  alertNotif(obselete: boolean): void {
    if (obselete) {
      this.toastr.success('L\'objectif a bien été désactivé.', 'Désactivation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    } else {
      this.toastr.success('L\'objectif a bien été activé.', 'Activation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    }
  }

}
