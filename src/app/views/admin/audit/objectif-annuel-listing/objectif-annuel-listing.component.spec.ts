import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectifAnnuelListingComponent } from './objectif-annuel-listing.component';

describe('ObjectifAnnuelListingComponent', () => {
  let component: ObjectifAnnuelListingComponent;
  let fixture: ComponentFixture<ObjectifAnnuelListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ObjectifAnnuelListingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectifAnnuelListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
