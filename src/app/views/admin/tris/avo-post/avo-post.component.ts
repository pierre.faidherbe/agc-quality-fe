import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {TrisService} from '../../../../services/tris/tris.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-avo-post',
  templateUrl: './avo-post.component.html',
  styleUrls: ['./avo-post.component.css']
})
export class AvoPostComponent implements OnInit {
  // @ts-ignore
  newAvo: Avo;
  nomLieu = new FormControl('', [Validators.required, Validators.minLength(3)]);

  constructor(
    private trisService: TrisService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.newAvo = new Avo(null, '', 2);
  }

  getErrorMessage(): string {
    if (this.nomLieu.hasError('required')) {
      return 'Le champs doit être rempli et avoir au moins 3 caractères';
    }
    if (this.nomLieu.hasError('minlenght')) {
      return 'Le champs doit avoir au moins 3 caractères';
    }
    return '';
  }

  save(): void {
    this.trisService.createAvo(this.newAvo).subscribe(res => {
      console.log(res);
    }, error => {
      console.log(error);
    }, () => {
    });
    this.alertNotif();
  }

  alertNotif(): void {
    this.toastr.success('L\'AVO a bien été enregistré.', 'Enregistrement', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }

}


export class Avo {
  constructor(
    public lieuAvoId: number | null,
    public nomLieu: string,
    // tslint:disable-next-line:variable-name
    public fk_typeTri_AVO: number,
  ) {
  }
}
