import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvoPostComponent } from './avo-post.component';

describe('AvoPostComponent', () => {
  let component: AvoPostComponent;
  let fixture: ComponentFixture<AvoPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvoPostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvoPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
