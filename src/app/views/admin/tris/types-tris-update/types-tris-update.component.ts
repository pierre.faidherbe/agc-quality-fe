import {Component, OnInit} from '@angular/core';
import {Tri, TrisService} from '../../../../services/tris/tris.service';
import {ActivatedRoute} from '@angular/router';
import {TypeTris} from '../types-tris-post/types-tris-post.component';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-types-tris-update',
  templateUrl: './types-tris-update.component.html',
  styleUrls: ['./types-tris-update.component.css']
})
export class TypesTrisUpdateComponent implements OnInit {
  // @ts-ignore
  private id: number;
  // @ts-ignore
  typeTriToUpdate: TypeTris;

  constructor(
    private trisService: TrisService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.typeTriToUpdate = new TypeTris('', null);
    this.id = parseInt(this.route.snapshot.paramMap.get('id') as string, 10);
    this.getTriToUpdate();
  }

  getTriToUpdate(): void {
    this.trisService.getTypeTrisById(this.id).subscribe(res => {
      this.typeTriToUpdate = res;
    });
  }

  save(): void {
    this.trisService.updateTypeTris(this.typeTriToUpdate).subscribe(res => {
        console.log(res);
      }, error => {
      },
      () => {
      });
    this.alertNotif();
  }

  alertNotif(): void {
    this.toastr.success('Le type de tri a bien été modifié.', 'Update', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }
}
