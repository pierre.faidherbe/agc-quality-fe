import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypesTrisUpdateComponent } from './types-tris-update.component';

describe('TypesTrisUpdateComponent', () => {
  let component: TypesTrisUpdateComponent;
  let fixture: ComponentFixture<TypesTrisUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypesTrisUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypesTrisUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
