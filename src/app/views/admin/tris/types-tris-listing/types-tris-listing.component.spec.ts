import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypesTrisListingComponent } from './types-tris-listing.component';

describe('TypesTrisListingComponent', () => {
  let component: TypesTrisListingComponent;
  let fixture: ComponentFixture<TypesTrisListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypesTrisListingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypesTrisListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
