import {Component, OnInit} from '@angular/core';
import {TrisService, TypeTris} from '../../../../services/tris/tris.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-types-tris-listing',
  templateUrl: './types-tris-listing.component.html',
  styleUrls: ['./types-tris-listing.component.css']
})
export class TypesTrisListingComponent implements OnInit {
  typeTriListing: TypeTrisView[] = [];
  temp: TypeTrisView[] = [];

  constructor(
    private triService: TrisService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.triService.getTypeTris().subscribe(res => {
        for (let entry of res) {
          this.typeTriListing.push({
            typeTriId: entry.typeTriId,
            typeTriNom: entry.typeTriNom,
            // @ts-ignore
            obsolete: entry.obsolete
          });
        }
      }, error => {
      },
      () => {
        this.typeTriListing = [...this.typeTriListing];
        this.temp = this.typeTriListing;
      });
  }

  filterData(event: any): void {
    const val = event.target.value.toLowerCase();
    // tslint:disable-next-line:only-arrow-functions
    const temp = this.temp.filter(function (d) {
      return d.typeTriNom.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.typeTriListing = temp;
  }

  changeStatut(obsolete: boolean, id: number): void {
    let jsonToSend = {obsolete, id};
    this.typeTriListing.length = 0;
    this.triService.deleteTypeTris(jsonToSend).subscribe(res => {
        this.getData();
      }, error => {
      },
      () => {
        this.alertNotif(obsolete)
      });
  }

  alertNotif(obsolete: boolean): void {
    if (obsolete) {
      this.toastr.success('Le type de tri a bien été désactivé.', 'Désactivation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    } else {
      this.toastr.success('Le type de tri a bien été activé.', 'Activation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    }
  }
}

export interface TypeTrisView {
  typeTriId: number | null;
  typeTriNom: string;
  obsolete: boolean;
}
