import {Component, OnInit} from '@angular/core';
import {Avo, TrisService} from '../../../../services/tris/tris.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-avo-listing',
  templateUrl: './avo-listing.component.html',
  styleUrls: ['./avo-listing.component.css']
})
export class AvoListingComponent implements OnInit {
  avoListing: AvoView[] = [];
  temp: AvoView[] = [];

  constructor(
    private trisService: TrisService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.trisService.getAvoLieux().subscribe(res => {
        for (let entry of res) {
          this.avoListing.push({
            nomLieu: entry.nomLieu,
            lieuAvoId: entry.lieuAvoId,
            fk_typeTri_AVO: entry.fk_typeTri_AVO,
            // @ts-ignore
            obsolete: entry.obsolete
          });
        }
      }, error => {
      },
      () => {
        this.avoListing = [...this.avoListing];
        this.temp = this.avoListing;
      });
  }

  filterData(event: any): void {
    const val = event.target.value.toLowerCase();
    // tslint:disable-next-line:only-arrow-functions
    const temp = this.temp.filter(function (d) {
      return d.nomLieu.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.avoListing = temp;
  }

  changeStatut(obsolete: boolean, id: number): void {
    let jsonToSend = {obsolete, id};
    this.avoListing.length = 0;
    this.trisService.deleteAVO(jsonToSend).subscribe(res => {
        this.getData();
      }, error => {
      },
      () => {
        this.alertNotif(obsolete)
      });
  }

  alertNotif(obsolete: boolean): void {
    if (obsolete) {
      this.toastr.success('L\'AVO a bien été désactivé.', 'Désactivation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    } else {
      this.toastr.success('L\'AVO a bien été activé.', 'Activation', {
        progressBar: true,
        positionClass: 'toast-top-right'
      })
    }
  }

}

export interface AvoView {
  lieuAvoId: number | null;
  nomLieu: string;
  fk_typeTri_AVO: number;
  obsolete: boolean;
}
