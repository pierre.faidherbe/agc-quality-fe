import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvoListingComponent } from './avo-listing.component';

describe('AvoListingComponent', () => {
  let component: AvoListingComponent;
  let fixture: ComponentFixture<AvoListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvoListingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvoListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
