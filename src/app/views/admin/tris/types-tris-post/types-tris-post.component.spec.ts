import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypesTrisPostComponent } from './types-tris-post.component';

describe('TypesTrisPostComponent', () => {
  let component: TypesTrisPostComponent;
  let fixture: ComponentFixture<TypesTrisPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypesTrisPostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypesTrisPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
