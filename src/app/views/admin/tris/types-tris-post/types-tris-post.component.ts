import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {TrisService} from '../../../../services/tris/tris.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-types-tris-post',
  templateUrl: './types-tris-post.component.html',
  styleUrls: ['./types-tris-post.component.css']
})
export class TypesTrisPostComponent implements OnInit {
  // @ts-ignore
  newTypeTri: TypeTris;
  typeTrisNom = new FormControl('', [Validators.required, Validators.minLength(3)]);

  constructor(
    private trisService: TrisService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.newTypeTri = new TypeTris('', null);
  }

  getErrorMessage(): string {
    if (this.typeTrisNom.hasError('required')) {
      return 'Le champs doit être rempli et avoir au moins 3 caractères';
    }
    if (this.typeTrisNom.hasError('minlenght')) {
      return 'Le champs doit avoir au moins 3 caractères';
    }
    return '';
  }

  save(): void {
    this.trisService.createTypeTris(this.newTypeTri).subscribe(res => {
      console.log(res);
    }, error => {
      console.log(error);
    }, () => {
    });
    this.alertNotif();
  }

  alertNotif(): void {
    this.toastr.success('Le type de tri a bien été enregistré.', 'Enregistrement', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }

}

export class TypeTris {
  constructor(
    public typeTriNom: string,
    public typeTriId: number | null,
  ) {
  }
}
