import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvoUpdateComponent } from './avo-update.component';

describe('AvoUpdateComponent', () => {
  let component: AvoUpdateComponent;
  let fixture: ComponentFixture<AvoUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvoUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvoUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
