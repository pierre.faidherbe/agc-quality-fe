import {Component, OnInit} from '@angular/core';
import {Avo, TrisService} from '../../../../services/tris/tris.service';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-avo-update',
  templateUrl: './avo-update.component.html',
  styleUrls: ['./avo-update.component.css']
})
export class AvoUpdateComponent implements OnInit {
  // @ts-ignore
  avoToUpdate: Avo;
  id = 0;

  constructor(
    private triService: TrisService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
  }

  ngOnInit(): void {
    this.id = parseInt(this.route.snapshot.paramMap.get('id') as string, 10);
    this.getAvoById();
  }

  getAvoById(): void {
    this.triService.getAvoLieuxById(this.id).subscribe(res => {
      this.avoToUpdate = res;
      console.log(res);
    });
  }

  save(): void {
    this.triService.updateAvo(this.avoToUpdate).subscribe(res => {
        console.log(res);
      }, error => {
      },
      () => {
      });
    this.alertNotif()
  }

  alertNotif(): void {
    this.toastr.success('L\'AVO a bien été modifié.', 'Update', {
      progressBar: true,
      positionClass: 'toast-top-right'
    })
  }

}
