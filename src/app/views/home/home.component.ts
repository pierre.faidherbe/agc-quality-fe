import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthenticationService, User } from 'src/app/authentication.service';
import { ChooseAuditDialogComponent } from 'src/app/components/Dialogs/choose-audit-dialog/choose-audit-dialog.component';
import { ChooseCrashDialogComponent } from 'src/app/components/Dialogs/choose-crash-dialog/choose-crash-dialog.component';
import { ChooseTriDialogComponent } from 'src/app/components/Dialogs/choose-tri-dialog/choose-tri-dialog.component';
import { DbService } from 'src/app/db.service';
import { Crash, CrashsService } from 'src/app/services/crashs/crashs.service';
import { TrisService } from 'src/app/services/tris/tris.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  auditsLength?: number;
  trisLength?: number;
  crashsLength?: number;
  openCrash?: Crash;
  currentUser?: User;

  constructor(
    private dbService: DbService,
    private dialog: MatDialog,
    private crashService: CrashsService,
    private triService: TrisService,
    private authService: AuthenticationService
  ) {}

  ngOnInit(): void {
    this.getOpenCrash();

    this.dbService
      .getAudits()
      .subscribe((audits) => (this.auditsLength = audits.length));

    this.triService
      .getTris()
      .subscribe((tris) => (this.trisLength = tris.length));

    this.authService
      .getCurrentUser()
      .subscribe((user) => (this.currentUser = user));
  }

  openChooseAuditDialog() {
    this.dialog.open(ChooseAuditDialogComponent);
  }
  openChooseCrashDialog() {
    this.dialog.open(ChooseCrashDialogComponent);
  }
  openChooseTriDialog() {
    this.dialog.open(ChooseTriDialogComponent);
  }

  getOpenCrash = () =>
    this.crashService.getCrashs().subscribe((crashs) => {
      this.crashsLength = crashs.length;
      const temp = crashs.filter((crash) => crash.statut);
      this.openCrash = temp[temp.length - 1];
    });
}
