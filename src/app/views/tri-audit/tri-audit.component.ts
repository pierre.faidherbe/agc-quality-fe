import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication.service';
import { CriteresFormTriComponent } from 'src/app/components/criteres-form-tri/criteres-form-tri.component';
import { TopFormTriComponent } from 'src/app/components/top-form-tri/top-form-tri.component';
import { Critere, DbService, TriToSend } from 'src/app/db.service';
import { Tri, TrisService } from 'src/app/services/tris/tris.service';

@Component({
  selector: 'app-tri-audit',
  templateUrl: './tri-audit.component.html',
  styleUrls: ['./tri-audit.component.css'],
})
export class TriAuditComponent implements OnInit {
  triReceived?: Tri;
  title: string = "Enregistrement d'un tri";

  constructor(
    private dbService: DbService,
    private triService: TrisService,
    private authService: AuthenticationService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const id = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    this.triService.getTri(id).subscribe((tri) => {
      this.receivedTri(tri);
    });
  }

  receivedTri(tri: Tri) {
    this.triReceived = tri;

    this.title = 'tri du ' + new Date(tri.dateDebut).toLocaleDateString();
  }

  private buildDate = (date: string): Date => {
    let today = new Date();

    const _hours = parseInt(date.substring(0, 2));
    const _minutes = parseInt(date.substring(3, 5));

    today.setHours(_hours, _minutes);

    return today;
  };

  async submitForm(
    appTopFormTri: TopFormTriComponent,
    appCriteresFormTri: CriteresFormTriComponent
  ) {
    const tempCriteres: any[] = [];

    let criteres = appCriteresFormTri.defautsForm.value;

    Object.keys(criteres).forEach((key) => {
      tempCriteres.push({
        FK_critereId: key,
        valueCritere: Object.values(criteres[key]).join(';'),
      });
    });

    const dateDeb = this.buildDate(
      appTopFormTri.formGroup.controls['heureDebut'].value
    );
    const dateFin = this.buildDate(
      appTopFormTri.formGroup.controls['heureFin'].value
    );

    const triToSend: TriToSend = {
      fk_typeTris: appTopFormTri.formGroup.controls['typeDeTri'].value,
      fk_LieuAVO: appTopFormTri.formGroup.controls['avo'].value || null,
      fk_articles:
        appTopFormTri.formGroup.controls['articles'].value['articleId'],
      fk_market: appTopFormTri.formGroup.controls['market'].value,
      fk_user: await this.authService
        .getCurrentUser()
        .toPromise()
        .then((user) => user?.idUser || 0),
      numGallia: appTopFormTri.formGroup.controls['nDeGalia'].value,
      numOS: appTopFormTri.formGroup.controls['nOS'].value,
      nbPieces: appCriteresFormTri.totalDeVerres,
      dateDeb: dateDeb,
      dateFin: dateFin,
      commentaire: 'test', //TODO,
      criteres: tempCriteres,
    };

    console.log(triToSend);

    this.dbService.postTriToSend(triToSend);

    window.alert('Tri envoyé');
    this.router.navigateByUrl('/home');
  }
}
