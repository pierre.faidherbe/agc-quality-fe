import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { fromEventPattern } from 'rxjs';
import {
  DbService,
  AuditToSend,
  Process,
  Critere,
  Audit,
} from 'src/app/db.service';
import { AnalyseFormAuditComponent } from 'src/app/components/analyse-form-audit/analyse-form-audit.component';
import { BottomFormComponent } from 'src/app/components/bottom-form/bottom-form.component';
import { TopFormAuditComponent } from 'src/app/components/top-form-audit/top-form-audit.component';
import { AuthenticationService } from 'src/app/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-enregistrement-audit',
  templateUrl: './enregistrement-audit.component.html',
  styleUrls: ['./enregistrement-audit.component.css'],
})
export class EnregistrementAuditComponent implements OnInit {
  selectedProcess?: Process;
  totalTotal: number = 100;
  rating?: string;
  auditReceived?: Audit;
  title: string = "Enregistrement d'un audit";

  constructor(
    private dbService: DbService,
    private authService: AuthenticationService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    const id = Number(this.activatedRoute.snapshot.paramMap.get('id'));

    if (id === 0) return; //TODO mieux faire

    this.dbService.getAudit(id).subscribe((audit) => {
      this.receivedAudit(audit);
    });
  }

  receivedAudit(audit: Audit) {
    this.auditReceived = audit;

    this.title = 'Audit du ' + new Date(audit.dateFin).toLocaleDateString();
  }

  /* -------------------------------------------------------------------------- */
  /*                                  onChanged                                 */
  /* -------------------------------------------------------------------------- */

  onLigneChanged(data: Process) {
    this.selectedProcess = data;
  }

  onTotalTotalChanged(data: any) {
    this.totalTotal = data;
  }

  onRatingChanged = (data: string) => {
    this.rating = data;
  };

  ///Building date
  private buildDate = (date: string): Date => {
    let today = new Date();

    const _hours = parseInt(date.substring(0, 2));
    const _minutes = parseInt(date.substring(3, 5));

    today.setHours(_hours, _minutes);

    return today;
  };

  private replacer(cle: any, valeurs: Critere[]) {
    console.log(valeurs);

    return valeurs.forEach(
      (critere) =>
        (critere.valueCritere = (critere.valueCritere as any[]).join(';'))
    );
  }

  async submitForm(
    topForm: TopFormAuditComponent,
    criteresForm: AnalyseFormAuditComponent,
    bottomForm: BottomFormComponent
  ) {
    const date = this.buildDate(topForm.formGroup.controls['auditTime'].value);
    let criteres = criteresForm.formGroup.value;

    ///Building criteres
    let criteresArray = Object.keys(criteres).map<Critere>((i) => criteres[i]);

    console.log(criteresArray);

    JSON.stringify(criteresArray, this.replacer);

    console.log(criteresArray);

    if (
      topForm.submitForm() &&
      criteresForm.submitForm() &&
      bottomForm.submitForm()
    ) {
      let auditToSend: AuditToSend = {
        dateDeb: date,
        fk_articles:
          topForm.formGroup.controls['selectedArticle'].value['articleId'],

        fk_user: await this.authService
          .getCurrentUser()
          .toPromise()
          .then((user) => user.idUser),
        four: topForm.formGroup.controls['selectedAuditFour'].value,
        numContainer: topForm.formGroup.controls['nContainer'].value,
        objectifAnnuel: await this.dbService
          .getObjectifAnnuel()
          .toPromise()
          .then((objectif) => objectif.objectifId),
        resultPourcentage: this.totalTotal,
        rating: this.rating!,
        totalVerres: 0, //TODO important ?
        commentaireGeneral: bottomForm.formGroup.controls['commentaires'].value,
        criteres: criteresArray,
        action: bottomForm.formGroup.controls['actions'].value,
      };

      this.dbService.postAuditToSend(auditToSend);

      window.alert('Audit envoyé');
      this.router.navigateByUrl('/home');
    } else {
      window.alert("Vous n'avez pas rempli tous les formulaires");
      console.log('no form');
    }
  }
}
