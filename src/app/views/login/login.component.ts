import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService, User } from 'src/app/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  be = new FormControl();

  constructor(
    private authService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.listenToAuth();
  }

  login() {
    console.log('login');

    this.authService.login(parseInt(this.be.value));
  }
  listenToAuth() {
    this.authService.onConnection.subscribe(() =>
      this.router.navigate(['/home'])
    );
    this.authService.onConnectionError.subscribe(() =>
      this.be.setErrors({ user: true })
    );
  }
}
