import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { map, startWith } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/authentication.service';
import { DialogZoneComponent } from 'src/app/components/Dialogs/dialog-zone/dialog-zone.component';
import { Article, CodeArticle, DbService, Modele } from 'src/app/db.service';
import {
  CrashsService,
  CrashToSend,
  Ligne,
  SousMachine,
  Zone,
} from 'src/app/services/crashs/crashs.service';

@Component({
  selector: 'app-crash-qualite',
  templateUrl: './crash-qualite.component.html',
  styleUrls: ['./crash-qualite.component.css'],
})
export class CrashQualiteComponent implements OnInit {
  formGroup: FormGroup = new FormGroup({});
  selectedLigne?: Ligne;
  selectedMachine?: SousMachine;

  modeles: Modele[] = [];
  articles: Article[] = [];
  zones: Zone[] = [];
  filteredArticles: Article[] = [];

  constructor(
    private matDialog: MatDialog,
    private formBuilder: FormBuilder,
    private dbService: DbService,
    private crashService: CrashsService,
    private authService: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.buildFormulaire();
    this.getModeles();
    this.getArticles();

    this.listenToArticleComboBox();
    this.listenToModeleComboBox();
    this.listenToCodesArticles();
  }

  onClickToOpenDialog() {
    const ref = this.matDialog.open(DialogZoneComponent, {});
    ref.afterClosed().subscribe((data) => {
      this.selectedLigne = data[0];
      this.selectedMachine = data[1];
      this.formGroup.controls['lieuDeDetection'].setValue(data);
      console.log(this.selectedLigne);
    });
  }

  buildFormulaire() {
    this.formGroup = this.formBuilder.group({
      dateCrash: [],
      lieuDeDetection: [],
      nbPieces: [],
      articles: [],
      modele: [],
      partNumber: [],
      description: [],
      piecesJointes: [],
    });
  }

  getModeles = () => {
    this.dbService
      .getModeles()
      .subscribe((modeles: Modele[]) => (this.modeles = modeles));
  };

  getArticles = () => {
    this.dbService.getArticles().subscribe((data: Article[]) => {
      this.filteredArticles = data;
      return (this.articles = data);
    });
  };

  listenToArticleComboBox(): any {
    this.formGroup.controls['articles'].valueChanges.subscribe(
      (data: Article) => {
        if (
          this.articles.includes(data) ||
          this.filteredArticles.includes(data)
        ) {
          this.dbService
            .getCodeArticle(data.codeArticle)
            .subscribe((data: Article) => {
              //this.loading = true;
              this.formGroup.controls['modele'].setValue(
                this.modeles.find((x) => x.modeleId == data.modeleId),
                { emitEvent: false }
              );
              this.formGroup.controls['partNumber'].setValue(data.partNumber);

              //this.loading = false;
            });
        }
      }
    );
  }

  listenToModeleComboBox() {
    this.formGroup.controls['modele'].valueChanges.subscribe((data: Modele) => {
      //this.loading = true;
      console.log(`listening for ${data}`);
      this.dbService
        .getCodeArticlesPerModele(data)
        .subscribe((data: Article[]) => {
          this.filteredArticles = data;
          //this.loading = false;
        });
      this.formGroup.controls['articles'].reset();
      this.formGroup.controls['partNumber'].reset();
    });
  }

  listenToCodesArticles = () => {
    //TODO: doublon
    this.formGroup.controls['articles'].valueChanges
      .pipe(
        startWith(''),
        map((id) => this._filter(id))
      )
      .subscribe((result) => (this.filteredArticles = result));
  };

  _filter = (value: number): Article[] => {
    const filterValue = value || undefined;

    return this.articles.filter((option: Article) =>
      option.codeArticle.toString().startsWith(filterValue?.toString() || '')
    );
  };

  getOptionText(option: CodeArticle) {
    return option ? option.codeArticle.toString() : '';
  }

  async onSubmit() {
    if (this.formGroup.valid) {
      const article: Article = this.formGroup.controls['articles'].value;

      const crashToSend: CrashToSend = {
        dateCrash: this.formGroup.controls['dateCrash'].value,
        description: this.formGroup.controls['description'].value,
        detectionCrash: {
          fk_ligne: this.selectedLigne!.ligneId,
          fk_machine: this.selectedMachine?.machineId || null,
        },
        fk_article: article.articleId,
        fk_user: await this.authService
          .getCurrentUser()
          .toPromise()
          .then((user) => user.idUser),
        nbPieces: this.formGroup.controls['nbPieces'].value,
        piecesJointes: this.formGroup.controls['piecesJointes'].value,
      };

      this.crashService.postCrashQualite(crashToSend);

      window.alert('Crash envoyé');
      this.router.navigateByUrl('/home');
    } else {
      window.alert('Veuillez remplir tous les champs  ');
    }
  }

  getLigneOrMachineValue = (ligneOrMachine: any[]) => {
    return ligneOrMachine != undefined
      ? (ligneOrMachine[1] as SousMachine).machine ||
          (ligneOrMachine[0] as Ligne).ligne
      : undefined;
  };
}
