import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { API_URL } from './app.globalConstante';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  @Output() onConnection = new EventEmitter();
  @Output() onConnectionError = new EventEmitter<string>();

  constructor(private http: HttpClient) {}

  getCurrentUser(): Observable<User> {
    const user: User = JSON.parse(sessionStorage.getItem('user')!);

    return of(user as User);
  }

  login(be: number) {
    console.log(be);

    return this.http
      .get<User>(`${API_URL}/users/${be}`)
      .subscribe((user: User) => {
        if (user != undefined) {
          sessionStorage.setItem('isAuth', 'true');
          sessionStorage.setItem('user', JSON.stringify(user));
          this.onConnection.emit();
        } else {
          this.onConnectionError.emit('error user not found');
        }
      });
  }
}
export interface User {
  idUser: number;
  nom: string;
  prenom: string;
  FK_role: number;
  FK_equipe: number;
  BE: number;
  tracabilite: any; //TODO To check
}
