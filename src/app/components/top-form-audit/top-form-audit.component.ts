import { DatePipe } from '@angular/common';
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  Article,
  DbService,
  Process,
  Modele,
  CodeArticle,
  Audit,
} from 'src/app/db.service';
import { map, startWith } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthenticationService, User } from 'src/app/authentication.service';

@Component({
  selector: 'app-top-form-audit',
  templateUrl: './top-form-audit.component.html',
  styleUrls: ['./top-form-audit.component.css'],
})
export class TopFormAuditComponent implements OnInit, OnChanges {
  @Output() onLigneChanged = new EventEmitter<Process>();
  @Output() onFormChanged = new EventEmitter<FormGroup>();
  today = new Date();

  @Input() auditReceived?: Audit;

  formGroup = new FormGroup({
    auditTime: new FormControl(this.datePipe.transform(this.today, 'HH:mm')),
    selectedAuditModele: new FormControl(),
    selectedArticle: new FormControl(),
    selectedPause: new FormControl(),
    selectedAuditLigne: new FormControl(),
    selectedAuditFour: new FormControl(),
    selectedPartNumber: new FormControl(),
    selectedTeinte: new FormControl(),
    currentAuditeur: new FormControl(),
    currentEquipe: new FormControl(),
    nContainer: new FormControl(),
    tracabilite: new FormControl(),
  });

  auditModeles: Modele[] = [];
  auditProcess: Process[] = [];
  pauses: string[] = ['Matin', 'Après-midi', 'Nuit'];
  fours = ['FL1', 'FL2', 'FL3', 'FL4'];
  articles: Article[] = [];
  filteredArticles: Article[] = [];

  constructor(
    public datePipe: DatePipe,
    private dbService: DbService,
    private authService: AuthenticationService,
    public snackbar: MatSnackBar
  ) {}

  loading = false;

  ngOnChanges(changes: SimpleChanges): void {
    let change = changes['auditReceived'];

    if (!change.firstChange) {
      this.auditReceived = change.currentValue;
      this.buildFromAuditReceived();
    }
  }

  buildFromAuditReceived = () => {
    //TODO manque ligne et modele
    this.formGroup.disable();
    const modele: Modele = { modele: 'test', modeleId: 1 };

    this.formGroup.controls['auditTime'].setValue(
      this.datePipe.transform(
        new Date(this.auditReceived?.dateFin || this.today),
        'HH:mm'
      )
    );

    this.formGroup.controls['selectedAuditModele'].setValue(modele.modele);
    this.formGroup.controls['selectedAuditFour'].setValue(
      this.auditReceived?.four
    );

    this.formGroup.controls['selectedArticle'].setValue(
      this.auditReceived?.article
    );
    this.formGroup.controls['nContainer'].setValue(
      this.auditReceived?.numContainer
    );
    this.formGroup.controls['selectedPartNumber'].setValue(
      this.auditReceived?.article.partNumber
    );

    this.loading = false;
  };

  ngOnInit(): void {
    this.getCodesArticle();
    this.getAuditProcess();
    this.getAuthentication();
    this.getAuditModeles();

    this.formGroup.controls['selectedPause'].setValue(
      this.calculatePauseTime()
    );

    this.listenToArticleComboBox();
    this.listenToModeleComboBox();
    this.listenToForm();
    this.listenToCodesArticles();
    this.listenToLigneSelected();
    //todo listen to four if FL4 -> HUD
  }

  submitForm(): boolean {
    if (this.formGroup.valid) {
      return true;
    } else {
      this.formGroup.markAllAsTouched();
      return false;
    }
  }

  calculatePauseTime(): string {
    let now = new Date();

    if (now.getHours() >= 6 && now.getHours() < 14) return this.pauses[0];
    else if (now.getHours() >= 14 && now.getHours() < 22) return this.pauses[1];
    else return this.pauses[2];
  }

  /* -------------------------------------------------------------------------- */
  /*                                    GETS                                    */
  /* -------------------------------------------------------------------------- */

  getAuthentication(): void {
    this.authService.getCurrentUser().subscribe((data: User) => {
      this.formGroup.controls['tracabilite'].setValue(data.tracabilite);
    });
  }

  getAuditProcess(): void {
    this.dbService
      .getAuditProcess()
      .subscribe((data: Process[]) => (this.auditProcess = data));
  }

  getCodesArticle(): void {
    this.dbService.getArticles().subscribe((data: Article[]) => {
      this.filteredArticles = data;
      return (this.articles = data);
    });
  }

  getAuditModeles(): void {
    this.dbService.getModeles().subscribe((data: Modele[]) => {
      this.auditModeles = data;
    });
  }

  /* -------------------------------------------------------------------------- */
  /*                                  Listening                                 */
  /* -------------------------------------------------------------------------- */

  listenToModeleComboBox() {
    this.formGroup.controls['selectedAuditModele'].valueChanges.subscribe(
      (data: Modele) => {
        this.loading = true;
        this.dbService
          .getCodeArticlesPerModele(data)
          .subscribe((data: Article[]) => {
            this.filteredArticles = data;
            this.loading = false;
          });
        this.formGroup.controls['selectedArticle'].reset();
        this.formGroup.controls['selectedPartNumber'].reset();
      },
      (err) => {
        console.log(err);
        this.loading = false;
      }
    );
  }

  /*   listenToLigneComboBox() {
    this.formGroup.controls['selectedAuditLigne'].valueChanges.subscribe(
      (data) => {
        this.onLigneChanged.emit(data);
      }
    );
  } */

  listenToArticleComboBox(): any {
    this.formGroup.controls['selectedArticle'].valueChanges.subscribe(
      (data: Article) => {
        if (
          this.articles.includes(data) ||
          this.filteredArticles.includes(data)
        ) {
          this.dbService.getCodeArticle(data.codeArticle).subscribe(
            (data: Article) => {
              this.loading = true;
              this.formGroup.controls['selectedAuditModele'].setValue(
                this.auditModeles.find((x) => x.modeleId == data.modeleId),
                { emitEvent: false }
              );
              console.log(data);
              this.formGroup.controls['selectedPartNumber'].setValue(
                data.partNumber
              );
              this.formGroup.controls['selectedTeinte'].setValue(
                data.teinte || 'mock teinte'
              );
              this.loading = false;
            },
            (err: HttpErrorResponse) => {
              console.log(err);
              this.loading = false;
              this.showErrDialog(err.message);
            }
          );
        }
      }
    );
  }

  listenToForm() {
    this.formGroup.valueChanges.subscribe((form) => {
      return this.onFormChanged.emit(form);
    });
  }

  listenToCodesArticles = () => {
    //TODO: doublon
    this.formGroup.controls['selectedArticle'].valueChanges
      .pipe(
        startWith(''),
        map((id) => this._filter(id))
      )
      .subscribe((result) => (this.filteredArticles = result));
  };

  _filter = (value: number): Article[] => {
    const filterValue = value || undefined;

    return this.articles.filter((option: Article) =>
      option.codeArticle.toString().startsWith(filterValue?.toString() || '')
    );
  };

  listenToLigneSelected() {
    //TODO: tout refaire
    let fourFormControl = this.formGroup.controls['selectedAuditFour'];
    let ligneFormControl = this.formGroup.controls['selectedAuditLigne'];

    ligneFormControl.valueChanges.subscribe((data: Process) => {
      if (data.nomProcess == 'pre-process') {
        fourFormControl.enable();

        this.fours = ['FL1', 'FL2', 'FL3'];
      } else if (data.nomProcess == 'multiprint') {
        fourFormControl.enable();

        this.fours = ['FL4'];
        fourFormControl.setValue(this.fours[0]);
      } else {
        fourFormControl.reset();
        fourFormControl.disable();
      }
      this.onLigneChanged.emit(data);
    });
  }

  showErrDialog(err: string) {
    this.snackbar.open(err, undefined, { duration: 4000 });
  }

  getOptionText(option: CodeArticle) {
    return option ? option.codeArticle.toString() : '';
  }
}
