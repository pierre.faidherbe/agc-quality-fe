import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatRadioChange } from '@angular/material/radio';
import { map, startWith } from 'rxjs/operators';
import { Article, CodeArticle, DbService, Modele } from 'src/app/db.service';
import { Market, TypeTris } from 'src/app/services/tris/tris.service';
import { Avo, Tri, TrisService } from 'src/app/services/tris/tris.service';

@Component({
  selector: 'app-top-form-tri',
  templateUrl: './top-form-tri.component.html',
  styleUrls: ['./top-form-tri.component.css'],
})
export class TopFormTriComponent implements OnInit, OnChanges {
  @Input() triReceived?: Tri;

  formGroup = new FormGroup({
    heureDebut: new FormControl(),
    heureFin: new FormControl(),
    modeles: new FormControl(),
    articles: new FormControl(),
    typeDeTri: new FormControl(),
    nDeGalia: new FormControl(),
    motifDeTri: new FormControl(),
    nOS: new FormControl(),
    market: new FormControl(),
    avo: new FormControl(),
  });

  modeles: Modele[] = [];
  articles: Article[] = [];
  filteredArticles: Article[] = [];

  typesDeTri: TypeTris[] = [];
  avoLieux: Avo[] = [];

  markets: Market[] = [];

  selectedTypeTriId?: number;

  loading = false;

  constructor(private dbService: DbService, private triService: TrisService) {}

  ngOnChanges(changes: SimpleChanges): void {
    let change = changes['triReceived'];

    if (!change.firstChange) {
      this.triReceived = change.currentValue;
      this.buildFromTriReceived();
    }
  }

  buildFromTriReceived = () => {
    const heureDeb = new Date(this.triReceived?.dateDebut || '')
      .toLocaleTimeString()
      .slice(0, -3);
    const heureFin = new Date(this.triReceived?.dateFin || '')
      .toLocaleTimeString()
      .slice(0, -3);

    this.formGroup.controls['heureDebut'].setValue(heureDeb);
    this.formGroup.controls['heureFin'].setValue(heureFin);
    this.formGroup.controls['nDeGalia'].setValue(this.triReceived?.numGallia);
    this.formGroup.controls['nOS'].setValue(this.triReceived?.numOS);
    this.formGroup.controls['articles'].setValue(
      this.triReceived?.article.codeArticle
    );
    this.formGroup.controls['modeles'].setValue(
      this.triReceived?.article.modele
    );
    this.formGroup.controls['market'].setValue(
      this.triReceived?.market.marketId
    );
    this.formGroup.controls['typeDeTri'].setValue(
      this.triReceived?.typeTri.typeTriId
    );
    this.formGroup.controls['avo'].setValue(this.triReceived?.avo?.lieuAvoId);
    this.formGroup.disable();

    this.loading = false;
  };

  ngOnInit(): void {
    this.getModeles();
    this.getArticles();
    this.getTypesDeTri();
    this.getAvoLieux();
    this.getMarkets();
    this.listenToCodesArticles();
    this.listenToArticleComboBox();
    this.listenToModeleComboBox();
  }

  /* -------------------------------------------------------------------------- */
  /*                                    GETS                                    */
  /* -------------------------------------------------------------------------- */

  getMarkets = () =>
    this.triService
      .getMarkets()
      .subscribe((markets) => (this.markets = markets));

  getTypesDeTri = () =>
    this.dbService
      .getTypesTris()
      .subscribe((tris: TypeTris[]) => (this.typesDeTri = tris));

  getModeles = () => {
    this.dbService
      .getModeles()
      .subscribe((modeles: Modele[]) => (this.modeles = modeles));
  };

  getArticles = () => {
    this.dbService.getArticles().subscribe((data: Article[]) => {
      this.filteredArticles = data;
      return (this.articles = data);
    });
  };

  getAvoLieux = () => {
    this.triService.getAvoLieux().subscribe((lieux) => (this.avoLieux = lieux)); //ng for in html
  };
  /* -------------------------------------------------------------------------- */
  /*                                   Listen                                   */
  /* -------------------------------------------------------------------------- */

  /*   listenToJourneeComplete = () => {
    this.formGroup.controls['journeeComplete'].valueChanges.subscribe(
      (data: boolean) => {
        if (data) {
          this.formGroup.controls['heureDebut'].disable();
          this.formGroup.controls['heureFin'].disable();
        } else {
          this.formGroup.controls['heureDebut'].enable();
          this.formGroup.controls['heureFin'].enable();
        }
      }
    );
  }; */

  listenToModeleComboBox() {
    this.formGroup.controls['modeles'].valueChanges.subscribe(
      (data: Modele) => {
        if (data.modeleId !== undefined) {
          this.loading = true;
          this.dbService
            .getCodeArticlesPerModele(data)
            .subscribe((data: Article[]) => {
              this.filteredArticles = data;
              this.loading = false;
            });
          this.formGroup.controls['articles'].reset();
        }
        //this.formGroup.controls['selectedPartNumber'].reset();
      },
      (err) => {
        console.log(err);
        this.loading = false;
      }
    );
  }

  listenToArticleComboBox(): any {
    this.formGroup.controls['articles'].valueChanges.subscribe(
      (data: Article) => {
        if (
          this.articles.includes(data) ||
          this.filteredArticles.includes(data)
        ) {
          this.dbService
            .getCodeArticle(data.codeArticle)
            .subscribe((data: Article) => {
              this.loading = true;
              this.formGroup.controls['modeles'].setValue(
                this.modeles.find((x) => x.modeleId == data.modeleId),
                { emitEvent: false }
              );
              this.loading = false;
            });
        }
      }
    );
  }

  listenToCodesArticles = () => {
    //TODO: doublon
    this.formGroup.controls['articles'].valueChanges
      .pipe(
        startWith(''),
        map((id) => this._filter(id))
      )
      .subscribe((result) => (this.filteredArticles = result));
  };

  _filter = (value: number): Article[] => {
    const filterValue = value || undefined;

    return this.articles.filter((option: Article) =>
      option.codeArticle.toString().startsWith(filterValue?.toString() || '')
    );
  };

  onTypeDeTriChanged(change: MatRadioChange) {
    this.selectedTypeTriId = change.value;
  }
  getOptionText(option: CodeArticle) {
    console.log(option);

    if (typeof option === 'number') return option;
    else if (option !== null) {
      console.log('object');

      return option.codeArticle.toString();
    } else return '';
  }
}
