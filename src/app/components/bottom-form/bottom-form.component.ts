import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Audit } from 'src/app/db.service';

@Component({
  selector: 'app-bottom-form',
  templateUrl: './bottom-form.component.html',
  styleUrls: ['./bottom-form.component.css'],
})
export class BottomFormComponent implements OnInit, OnChanges {
  /* ---------------------------------- vars ---------------------------------- */

  @Input() totalTotal!: number;

  objectifAnnuel: number = 98;

  @Output() onEnregistrerClick = new EventEmitter();

  rating?: string;

  @Output() onRatingChanged = new EventEmitter();

  @Input() auditReceived?: Audit;

  formGroup = new FormGroup({
    resultat: new FormControl(),
    actions: new FormControl(),
    commentaires: new FormControl(),
  });

  /* ---------------------------------- init ---------------------------------- */

  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {
    this.rating =
      this.totalTotal >= 95
        ? 'A'
        : this.totalTotal >= 90 && this.totalTotal < 95
        ? 'B'
        : 'C';

    this.formGroup.controls['resultat'].setValue(this.totalTotal);

    let change = changes['auditReceived'];

    if (change != undefined && !change.firstChange) {
      this.auditReceived = change.currentValue;
      console.log(this.auditReceived);

      this.totalTotal = this.auditReceived?.pourcentageResultat!;
      this.formGroup.controls['actions'].setValue(this.auditReceived?.action);
      this.formGroup.disable();
      this.formGroup.controls['commentaires'].setValue(
        this.auditReceived?.commentaireGeneral
      );
      this.rating = this.auditReceived?.rating;
    }
    this.ratingChanged();
  }

  ngOnInit(): void {}

  /* ----------------------------------- fct ---------------------------------- */

  enregistrerClick() {
    this.onEnregistrerClick.emit();
  }

  ratingChanged = () => {
    if (this.rating == 'A') this.formGroup.controls['actions'].disable();
    else if (this.auditReceived == undefined)
      this.formGroup.controls['actions'].enable();

    this.onRatingChanged.emit(this.rating);
  };

  submitForm(): boolean {
    if (this.formGroup.valid) {
      return true;
    } else {
      this.formGroup.markAllAsTouched();
      return false;
    }
  }
}
