import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
} from '@angular/forms';
import { DbService, Critere, Process, Audit } from 'src/app/db.service';

@Component({
  selector: 'app-analyse-form-audit',
  templateUrl: './analyse-form-audit.component.html',
  styleUrls: ['./analyse-form-audit.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnalyseFormAuditComponent
  implements OnInit, OnChanges, AfterViewInit {
  @Input() selectedProcess?: Process;
  @Input() auditReceived?: Audit;

  @Output() onTotalTotalChanged = new EventEmitter<number>();
  @Output() onFormChanged = new EventEmitter<FormGroup>();
  criteres: Critere[] = [];
  totalTotal?: number;
  totalVerres?: number;
  loading = false;
  formGroup: FormGroup = new FormGroup({});
  criteresForm: FormGroup = new FormGroup({});
  rowGroupMetadata: any;

  valuesCritere = (id: number): FormArray =>
    this.formGroup.get(`${id}.valueCritere`) as FormArray;

  constructor(
    private dbService: DbService,
    private _formBuilder: FormBuilder,
    private cdRef: ChangeDetectorRef
  ) {}

  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.selectedProcess != undefined) {
      this.loading = true;
      this.dbService
        .getCriteres(this.selectedProcess)
        .subscribe((data: Critere[]) => {
          this.criteres = data;
          this.buildFormulaire();
          this.onTotalTotalChanged.emit(100);
          this.loading = false;
        });
    }

    let change = changes['auditReceived'];

    if (change != undefined && !change.firstChange) {
      this.auditReceived = change.currentValue;
      console.log(this.auditReceived);

      this.criteres = this.auditReceived?.criteres || [];
      this.buildFormulaire();
      this.formGroup.disable();
    }
  }
  ngOnInit(): void {}

  listenToForm() {
    this.criteresForm.valueChanges.subscribe((form) => {
      return this.onFormChanged.emit(form);
    });
    console.log('listening to analyse form');
  }

  buildFormulaire = () => {
    console.log('rebuilding form');
    this.formGroup = new FormGroup({});
    this.criteres.forEach((critere: Critere) => {
      this.formGroup.setControl(
        critere.critereId.toString(),
        new FormGroup({
          FK_critereId: new FormControl(critere.FK_critereId),
          observation: new FormControl(critere.typeObservation),
          valueCritere:
            this.auditReceived == null
              ? new FormArray(
                  Array.from(
                    critere
                      .typeObservation!.split(';')
                      .map((x) => new FormControl())
                  )
                )
              : new FormArray(
                  Array.from(
                    String(critere.valueCritere)
                      .split(';')
                      .map((x) => new FormControl(x))
                  )
                ),
        })
      );
    });
    //this.formGroup.addControl('criteres', this.criteresForm);
    this.listenToForm();
    this.updateRowGroupMetaData();
    console.log(this.formGroup);
  };

  submitForm(): boolean {
    if (this.criteresForm.valid && this.formGroup.valid) {
      return true;
    } else {
      this.criteresForm.markAllAsTouched();
      console.log(this.criteresForm);
      return false;
    }
  }

  demeriteChanged(event: any, critere: Critere, at: number) {
    let totalTotal = 0;

    if (critere.nbOfDemerites == undefined)
      critere.nbOfDemerites = new Map<number, number>();

    critere.nbOfDemerites.set(at, parseInt(event.target.value));

    this.criteres.forEach((data: Critere) =>
      data.nbOfDemerites?.forEach((key, value) => {
        totalTotal += key * value || 0;
      })
    );
    this.totalTotal = 100 - totalTotal;

    this.onTotalTotalChanged.emit(this.totalTotal);
  }

  demeriteTotal(critere: Critere): number | undefined {
    if (critere.nbOfDemerites == undefined) return undefined;
    return Array.from(critere.nbOfDemerites.values()).reduce((a, b) => a + b);
  }

  onSort() {
    this.updateRowGroupMetaData();
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};

    if (this.criteres) {
      for (let i = 0; i < this.criteres.length; i++) {
        let rowData = this.criteres[i];
        let type = rowData.type || '';

        if (i == 0) {
          this.rowGroupMetadata[type] = { index: 0, size: 1 };
        } else {
          let previousRowData = this.criteres[i - 1];
          let previousRowGroup = previousRowData.type;
          if (type === previousRowGroup) this.rowGroupMetadata[type].size++;
          else this.rowGroupMetadata[type] = { index: i, size: 1 };
        }
      }
    }
  }
}
