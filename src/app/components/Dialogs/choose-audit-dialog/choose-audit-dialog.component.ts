import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Audit, DbService } from 'src/app/db.service';

@Component({
  selector: 'app-choose-audit-dialog',
  templateUrl: './choose-audit-dialog.component.html',
  styleUrls: ['./choose-audit-dialog.component.css'],
})
export class ChooseAuditDialogComponent implements OnInit {
  audits: Audit[] = [];
  choosenAudit = new FormControl({}, Validators.required);

  constructor(
    private dbService: DbService,
    private router: Router,
    public dialogRef: MatDialogRef<ChooseAuditDialogComponent>
  ) {
    dbService.getAudits().subscribe((audits) => (this.audits = audits));
  }

  ngOnInit(): void {}

  goToAudit() {
    let audit: Audit = this.choosenAudit.value;

    this.router.navigateByUrl(`/audit/${audit.auditId}`);
    this.dialogRef.close();
  }
}
