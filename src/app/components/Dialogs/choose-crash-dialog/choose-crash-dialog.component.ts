import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Crash, CrashsService } from 'src/app/services/crashs/crashs.service';

@Component({
  selector: 'app-choose-crash-dialog',
  templateUrl: './choose-crash-dialog.component.html',
  styleUrls: ['./choose-crash-dialog.component.css'],
})
export class ChooseCrashDialogComponent implements OnInit {
  crashs: Crash[] = [];
  choosenCrash = new FormControl({}, Validators.required);

  constructor(
    private crashService: CrashsService,
    private router: Router,
    public dialogRef: MatDialogRef<ChooseCrashDialogComponent>
  ) {}

  ngOnInit(): void {
    this.crashService
      .getCrashs()
      .subscribe(
        (crashs) =>
          (this.crashs = crashs.filter((crash) => crash.statut === false))
      );
  }

  goToCrash() {
    let crash: Crash = this.choosenCrash.value;

    this.router.navigateByUrl(`/crash/closed/${crash.crashQualiteId}`);
    this.dialogRef.close();
  }
}
