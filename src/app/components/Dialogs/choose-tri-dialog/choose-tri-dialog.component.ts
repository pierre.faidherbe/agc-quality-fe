import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CrashsService, Crash } from 'src/app/services/crashs/crashs.service';
import { Tri, TrisService } from 'src/app/services/tris/tris.service';

@Component({
  selector: 'app-choose-tri-dialog',
  templateUrl: './choose-tri-dialog.component.html',
  styleUrls: ['./choose-tri-dialog.component.css'],
})
export class ChooseTriDialogComponent implements OnInit {
  tris: Tri[] = [];
  choosenTri = new FormControl({}, Validators.required);

  constructor(
    private triService: TrisService,
    private router: Router,
    public dialogRef: MatDialogRef<ChooseTriDialogComponent>
  ) {}

  ngOnInit(): void {
    this.triService.getTris().subscribe((tris) => (this.tris = tris));
  }

  goToTri() {
    let tri: Tri = this.choosenTri.value;

    this.router.navigateByUrl(`/tri/${tri.triId}`);
    this.dialogRef.close();
  }
}
