import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import {
  Zone,
  Ligne,
  SousMachine,
  CrashsService,
} from 'src/app/services/crashs/crashs.service';
@Component({
  selector: 'app-dialog-zone',
  templateUrl: './dialog-zone.component.html',
  styleUrls: ['./dialog-zone.component.css'],
})
export class DialogZoneComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<DialogZoneComponent>,
    // tslint:disable-next-line:variable-name
    private _formBuilder: FormBuilder,
    private crashService: CrashsService
  ) {}

  zones: Zone[] = [];

  lignes: Ligne[] = [];

  sousMachines: SousMachine[] = [];

  filteredLigne: Ligne[] = [];
  filteredSousMachine: SousMachine[] = [];

  firstFormGroup: FormGroup = new FormGroup({});
  secondFormGroup: FormGroup = new FormGroup({});
  thirdFormGroup: FormGroup = new FormGroup({});

  horsLigneSelected = false;

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.buildFormulaire();
    this.filteredLigne = this.lignes;
    this.listenToFirstFormGroup();
    this.filteredSousMachine = this.sousMachines;
    this.listenToSecondFormGroup();
    this.getZones();
  }

  getZones = () =>
    this.crashService.getZones().subscribe((zones) => (this.zones = zones));

  getLignesByZone = (id: number) =>
    this.crashService
      .getLignesByZone(id)
      .subscribe((lignes) => (this.filteredLigne = lignes));

  getMachinesByLigne = (id: number) =>
    this.crashService
      .getMachinesByLigne(id)
      .subscribe((machines) => (this.filteredSousMachine = machines));

  // tslint:disable-next-line:typedef
  buildFormulaire() {
    this.firstFormGroup = this._formBuilder.group({
      firstControl: ['', Validators.required],
    });
    this.secondFormGroup = this._formBuilder.group({
      secondControl: ['', Validators.required],
    });
    this.thirdFormGroup = this._formBuilder.group({
      thirdControl: ['', Validators.required],
    });
  }

  // tslint:disable-next-line:typedef
  listenToFirstFormGroup() {
    this.firstFormGroup.controls['firstControl'].valueChanges.subscribe(
      (zone: Zone) => {
        if (zone.zoneId === 3) {
          this.horsLigneSelected = true;
        }
        // @ts-ignore
        return this.getLignesByZone(zone.zoneId);
      }
    );
  }

  // tslint:disable-next-line:typedef
  listenToSecondFormGroup() {
    this.secondFormGroup.controls['secondControl'].valueChanges.subscribe(
      (ligne: Ligne) => {
        // @ts-ignore
        this.getMachinesByLigne(ligne.ligneId);
      }
    );
  }

  // tslint:disable-next-line:typedef
  onConfirmerClick() {
    const ligne: Ligne = this.secondFormGroup.controls['secondControl'].value;
    const machine: SousMachine = this.thirdFormGroup.controls['thirdControl']
      .value;
    return this.dialogRef.close([ligne, machine]);
  }
}
