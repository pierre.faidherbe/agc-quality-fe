import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { AuthenticationService, User } from 'src/app/authentication.service';
import { SidenavService } from 'src/app/sidenav.service';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css'],
})
export class TopBarComponent implements OnInit {
  @Input() title?: string;
  currentUser?: User;

  constructor(
    private authenticationService: AuthenticationService,
    private sidenavService: SidenavService
  ) {}

  ngOnInit(): void {
    this.authenticationService
      .getCurrentUser()
      .subscribe((user: User) => (this.currentUser = user));
  }

  menuClicked() {
    this.sidenavService.toggle();
  }
}
