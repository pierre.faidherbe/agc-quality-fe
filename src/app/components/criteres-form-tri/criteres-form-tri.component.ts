import {
  AfterViewInit,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Critere, CritereDeTri, DbService } from 'src/app/db.service';
import { Tri } from 'src/app/services/tris/tris.service';

@Component({
  selector: 'app-criteres-form-tri',
  templateUrl: './criteres-form-tri.component.html',
  styleUrls: ['./criteres-form-tri.component.css'],
})
export class CriteresFormTriComponent implements OnInit, OnChanges {
  criteresDeTri: CritereDeTri[] = [];
  defautsForm = new FormGroup({});
  @Input() triReceived?: Tri;

  loading = false;
  totalDeVerres = 0;

  constructor(private dbService: DbService, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.getCriteresDeTri();
  }

  ngOnChanges(changes: SimpleChanges): void {
    let change = changes['triReceived'];

    if (change != undefined && !change.firstChange) {
      this.triReceived = change.currentValue;

      if (this.criteresDeTri === []) {
        this.criteresDeTri =
          this.triReceived?.criteres.map<CritereDeTri>(
            (x: Critere) => ({ valueCritere: x.valueCritere } as CritereDeTri)
          ) || [];
      }
      this.buildFormulaireFromTri();
      this.defautsForm.disable();
    }
  }

  /* -------------------------------------------------------------------------- */
  /*                                    GETS                                    */
  /* -------------------------------------------------------------------------- */

  getCriteresDeTri = () => {
    this.loading = true;
    this.dbService.getCriteresDeTri().subscribe((criteresDeTri) => {
      this.criteresDeTri = criteresDeTri;

      this.buildFormulaire();
      this.loading = false;
    });
  };

  buildFormulaireFromTri = () => {
    console.log('tri');
    console.log(this.triReceived);

    this.triReceived?.criteres.forEach((tri) => {
      const criteresArray = (tri.valueCritere as String).split(';');

      this.defautsForm.setControl(
        tri.FK_critereId.toString(),
        new FormGroup({
          fleurus: new FormControl(criteresArray[0]),
          AVO: new FormControl(criteresArray[1]),
          aReparer: new FormControl(criteresArray[2]),
          ARGMRP: new FormControl(criteresArray[3]),
          bonsApresRaccoFleurus: new FormControl(criteresArray[4]),
          bonsApresRaccoAVO: new FormControl(criteresArray[5]),
          bons: new FormControl(criteresArray[6]),
          //total: new FormControl(criteresArray[7]),
        })
      );
    });
    this.totalDeVerres = this.triReceived?.nbPieces || 0;
  };

  buildFormulaire = () => {
    console.log('no tri');

    this.criteresDeTri.forEach((critereDeTri) => {
      this.defautsForm.addControl(
        critereDeTri.critereId.toString(),
        new FormGroup({
          fleurus: new FormControl(0),
          AVO: new FormControl(0),
          aReparer: new FormControl(0),
          ARGMRP: new FormControl(0),
          bonsApresRaccoFleurus: new FormControl(0),
          bonsApresRaccoAVO: new FormControl(0),
          bons: new FormControl(0),
          //total: new FormControl(0),
        })
      );
    });

    setTimeout(() => {
      
      this.defautsForm.valueChanges.subscribe((data) => {
        let array: any[] = [];
        Array.from(Object.values(data)).forEach((ligne: any) => {
          array.push(Object.values(ligne).reduce((a: any, b: any) => a + b));
        });
        this.totalDeVerres = array.reduce((a: any, b: any) => a + b);
      });
    }, 300);

    //this.triForm.addControl('defauts', this.defautsForm);
  };
}
