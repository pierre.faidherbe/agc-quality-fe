import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Tri } from 'src/app/services/tris/tris.service';

@Component({
  selector: 'app-carto-tri',
  templateUrl: './carto-tri.component.html',
  styleUrls: ['./carto-tri.component.css'],
})
export class CartoTriComponent implements OnInit, OnChanges {
  @Output() onEnregistrerClick = new EventEmitter();
  @Input() triReceived?: Tri;

  commentaire = new FormControl('');

  ngOnChanges(changes: SimpleChanges): void {
    let change = changes['triReceived'];

    if (!change.firstChange) {
      this.triReceived = change.currentValue;
      this.commentaire.setValue(this.triReceived?.commentaireGeneral);
      this.commentaire.disable();
    }
  }

  constructor() {}

  ngOnInit(): void {}

  enregistrerClick = () => {
    this.onEnregistrerClick.emit();
  };
}
